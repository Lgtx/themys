#include "pqThemysAssistantMainWidget.h"

#include <QMenu>
#include <QMessageBox>
#include <QSortFilterProxyModel>
#include <QWidget>

#include "pqThemysAssistantBaseFilteringWidget.h"
#include "pqThemysAssistantBaseManager.h"
#include "pqThemysAssistantBaseModel.h"
#include "pqThemysAssistantStorageModel.h"
#include "pqThemysAssistantStudyModel.h"
#include "ui_pqThemysAssistantMainWidget.h"

//------------------------------------------------------------------------------
struct pqThemysAssistantMainWidget::pqInternals {
  Ui::pqThemysAssistantMainWidget Ui;
  pqThemysAssistantStorageModel* StorageModel;
  pqThemysAssistantBaseManager* BaseManager;
  pqThemysAssistantBaseModel* BaseModel;
  pqThemysAssistantStudyModel* StudyModel;
};

//-----------------------------------------------------------------------------
pqThemysAssistantMainWidget::pqThemysAssistantMainWidget(pqServer* server,
                                                         QWidget* parent)
    : Superclass(parent),
      Internals(new pqThemysAssistantMainWidget::pqInternals())
{
  auto& intern = *this->Internals;
  intern.Ui.setupUi(this);

  intern.StorageModel = new pqThemysAssistantStorageModel(server, this);
  intern.BaseManager =
      new pqThemysAssistantBaseManager(server, intern.StorageModel, this);
  intern.BaseModel = new pqThemysAssistantBaseModel(this);
  intern.StudyModel =
      new pqThemysAssistantStudyModel(server, intern.BaseManager, this);

  // Recover username and set it as command argument
  QString user = qgetenv("USER");
  if (user.isEmpty())
  {
    user = qgetenv("USERNAME");
  }
  intern.StorageModel->setCommandArgument(user);

  intern.Ui.baseFilteringWidget->Initialize(
      intern.BaseManager, intern.StorageModel, intern.BaseModel);

  QObject::connect(intern.Ui.toggleAssistant, &QToolButton::clicked, this,
                   &pqThemysAssistantMainWidget::toggleAssistantClicked);
  QObject::connect(intern.Ui.OK, &QPushButton::clicked, this,
                   &pqThemysAssistantMainWidget::acceptBases);
  QObject::connect(intern.Ui.Cancel, &QPushButton::clicked, this,
                   &pqThemysAssistantMainWidget::cancelClicked);

  intern.Ui.studyView->setSelectionBehavior(QAbstractItemView::SelectRows);
  intern.Ui.studyView->setSelectionMode(QAbstractItemView::ExtendedSelection);
  intern.Ui.studyView->setSortingEnabled(true);
  intern.Ui.studyView->header()->setSortIndicator(0, Qt::AscendingOrder);
  intern.Ui.studyView->setModel(intern.StudyModel->proxyModel());
  intern.Ui.studyView->setRootIsDecorated(false);
  QObject::connect(intern.Ui.studyView, &QTreeView::clicked, this,
                   &pqThemysAssistantMainWidget::onActivateStudy);
  QObject::connect(intern.StudyModel, &QAbstractItemModel::dataChanged,
                   [&]() { intern.Ui.studyView->resizeColumnToContents(0); });
  intern.Ui.studyView->resizeColumnToContents(0);

  QObject::connect(
      intern.BaseModel, &pqThemysAssistantBaseModel::studyBaseNameChanged,
      [&](int baseIdx, const QString& newName) {
        int studyIdx = intern.StudyModel->activeStudy();
        intern.StudyModel->renameBaseInStudy(studyIdx, baseIdx, newName);
      });

  QObject::connect(intern.Ui.createStudy, &QPushButton::clicked, this,
                   &pqThemysAssistantMainWidget::onCreateStudy);
  QObject::connect(intern.Ui.removeStudy, &QPushButton::clicked, this,
                   &pqThemysAssistantMainWidget::onRemoveSelectedStudy);
  QObject::connect(intern.Ui.clearStudies, &QPushButton::clicked, [&]() {
    QMessageBox confirm(nullptr);
    confirm.setText("All studies are about to be removed.\nProceed ?");
    confirm.addButton(QMessageBox::Ok);
    confirm.addButton(QMessageBox::Cancel);
    if (confirm.exec() == QMessageBox::Ok)
    {
      intern.StudyModel->clearStudies();
    }
  });

  QObject::connect(intern.Ui.baseFilteringWidget,
                   &pqThemysAssistantBaseFilteringWidget::preFilterBases,
                   [this]() { this->setCursor(Qt::BusyCursor); });
  QObject::connect(intern.Ui.baseFilteringWidget,
                   &pqThemysAssistantBaseFilteringWidget::showFilteredBases,
                   [&]() {
                     this->unsetCursor();
                     intern.Ui.modeNameLabel->setText("Filtered bases");
                     // This assume this get called once the model has already
                     // been updated. According to Qt documentation, this should
                     // the case since this connection is created last.
                     intern.Ui.Files->resizeColumnToContents(0);
                   });
  QObject::connect(intern.Ui.baseFilteringWidget,
                   &pqThemysAssistantBaseFilteringWidget::showRecentBases,
                   [&]() {
                     intern.Ui.modeNameLabel->setText("Recent bases");
                     // This assume this get called once the model has already
                     // been updated. According to Qt documentation, this should
                     // the case since this connection is created last.
                     intern.Ui.Files->resizeColumnToContents(0);
                   });
  QObject::connect(intern.StudyModel,
                   &pqThemysAssistantStudyModel::activeStudyRenamed,
                   [&](const QString& newName) {
                     intern.Ui.modeNameLabel->setText("Study " + newName);
                   });

  intern.Ui.Files->setContextMenuPolicy(Qt::CustomContextMenu);
  QObject::connect(intern.Ui.Files,
                   SIGNAL(customContextMenuRequested(const QPoint&)), this,
                   SLOT(onContextMenuRequested(const QPoint&)));
  intern.Ui.Files->setSelectionMode(QAbstractItemView::SingleSelection);
  intern.Ui.Files->setModel(intern.BaseModel->proxyModel());

  QObject::connect(
      intern.Ui.Files->selectionModel(),
      SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
      this, SLOT(fileSelectionChanged()));
}

//-----------------------------------------------------------------------------
pqThemysAssistantMainWidget::~pqThemysAssistantMainWidget() = default;

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::setToggleAssistantStatus(bool toggle)
{
  this->Internals->Ui.toggleAssistant->setChecked(toggle);
}

//-----------------------------------------------------------------------------
bool pqThemysAssistantMainWidget::acceptBases()
{
  auto& intern = *this->Internals;
  bool loaded = false;

  const QModelIndex current = intern.Ui.Files->selectionModel()->currentIndex();
  if (current.isValid())
  {
    int baseRow = intern.BaseModel->proxyModel()->mapToSource(current).row();
    const auto& base = intern.BaseModel->baseAt(baseRow);

    // when selecting a base a single file needs to be returned and
    // not the whole list of files.
    QString selectedBase;
    if (intern.BaseModel->currentState() == BaseDisplayState::FILTERED_BASE &&
        !base.FileList.empty())
    {
      selectedBase = base.FileList[0];
    } else
    {
      selectedBase = intern.BaseManager->GetBaseFirstFile(base.Identifier);
    }

    if (!selectedBase.isEmpty())
    {
      intern.BaseManager->AddToRecentBase(base);
      emit baseAccepted(selectedBase);
      loaded = true;
    }
  }

  return loaded;
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::onCreateStudy() const
{
  auto& intern = *this->Internals;
  int row = intern.StudyModel->addStudy("Study");
  QModelIndex sourceIndex = intern.StudyModel->index(row, 0, QModelIndex());

  // Edit the newly inserted study name
  intern.Ui.studyView->edit(
      intern.StudyModel->proxyModel()->mapFromSource(sourceIndex));
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::onRemoveSelectedStudy() const
{
  auto& intern = *this->Internals;
  auto* selection = intern.Ui.studyView->selectionModel();
  auto rowIndexes = selection->selectedRows();
  int size = selection->selectedRows().size();
  if (size < 1)
  {
    return;
  }
  // transform indices from proxy space to model space.
  std::transform(rowIndexes.begin(), rowIndexes.end(), rowIndexes.begin(),
                 [&](const QModelIndex& x) {
                   return intern.StudyModel->proxyModel()->mapToSource(x);
                 });

  QMessageBox confirm(nullptr);
  QString msg = "The following studies are about to be removed.\n";
  QChar bulletChar(0x2022);
  bool removingCurrent = false;
  for (const auto& idx : rowIndexes)
  {
    msg.append("    ")
        .append(bulletChar)
        .append("  ")
        .append(intern.StudyModel->data(idx.siblingAtColumn(0), Qt::DisplayRole)
                    .toString())
        .append("\n");

    removingCurrent |= (idx.row() == intern.StudyModel->activeStudy());
  }
  msg.append("Proceed ?");
  confirm.setText(msg);
  confirm.addButton(QMessageBox::Ok);
  confirm.addButton(QMessageBox::Cancel);
  if (confirm.exec() != QMessageBox::Ok)
  {
    return;
  }

  // Make sure indexes are reverse-sorted for easy removal.
  std::sort(rowIndexes.begin(), rowIndexes.end(),
            [](const QModelIndex& x, const QModelIndex& y) {
              return x.row() > y.row();
            });

  for (const auto& idx : rowIndexes)
  {
    intern.StudyModel->removeStudy(idx.row());
  }

  if (removingCurrent)
  {
    intern.StudyModel->markActiveStudy(-1);
    intern.BaseModel->setBases({}, BaseDisplayState::FILTERED_BASE);
    intern.Ui.modeNameLabel->setText("");
  }
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::onActivateStudy(const QModelIndex& idx) const
{
  auto& intern = *this->Internals;
  QModelIndex studyIdx = intern.StudyModel->proxyModel()->mapToSource(idx);
  intern.StudyModel->markActiveStudy(studyIdx.row());

  auto bases = intern.StudyModel->data(studyIdx, Qt::UserRole)
                   .value<QVector<assistant::Base>>();
  intern.BaseModel->setBases(bases, BaseDisplayState::STUDY);
  intern.Ui.Files->resizeColumnToContents(0);

  QString studyName =
      intern.StudyModel->data(studyIdx.siblingAtColumn(0), Qt::DisplayRole)
          .toString();
  intern.Ui.modeNameLabel->setText("Study " + studyName);
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::addSelectedBaseToStudy(
    const QModelIndex& studyIndex) const
{
  auto& intern = *this->Internals;
  QModelIndex baseIndex = intern.Ui.Files->selectionModel()->currentIndex();

  if (!studyIndex.isValid() || !baseIndex.isValid())
  {
    return;
  }

  int studyRow = studyIndex.row();
  int baseRow = intern.BaseModel->proxyModel()->mapToSource(baseIndex).row();

  assistant::Base base = intern.BaseModel->baseAt(baseRow);
  intern.StudyModel->addBaseToStudy(studyRow, base);
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::removeSelectedBaseFromActiveStudy() const
{
  auto& intern = *this->Internals;
  int studyRow = intern.StudyModel->activeStudy();
  QModelIndex baseIndex = intern.Ui.Files->selectionModel()->currentIndex();

  if (studyRow < 0 || !baseIndex.isValid())
  {
    return;
  }

  int baseRow = intern.BaseModel->proxyModel()->mapToSource(baseIndex).row();

  assistant::Base base = intern.BaseModel->baseAt(baseRow);
  intern.StudyModel->removeBaseFromStudy(studyRow, base);

  // Refresh Files
  auto bases = intern.StudyModel
                   ->data(intern.StudyModel->index(studyRow, 0, QModelIndex()),
                          Qt::UserRole)
                   .value<QVector<assistant::Base>>();
  intern.BaseModel->setBases(bases, BaseDisplayState::STUDY);
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::onContextMenuRequested(const QPoint& menuPos)
{
  auto& intern = *this->Internals;

  QMenu menu;
  menu.setObjectName("FileDialogContextMenu");
  QModelIndex proxyItemIndex =
      intern.Ui.Files->indexAt(menuPos).siblingAtColumn(0);

  bool baseIsSelected =
      intern.Ui.Files->selectionModel()->currentIndex().isValid();

  QMenu* saveToStudies = new QMenu("Add to study ...", &menu);
  saveToStudies->setEnabled(baseIsSelected);

  const int nStudy = intern.StudyModel->rowCount(QModelIndex());
  QList<QAction*> studyActions;
  for (int i = 0; i < nStudy; ++i)
  {
    QModelIndex studyIndex = intern.StudyModel->index(i, 0, QModelIndex());
    QString studyName =
        intern.StudyModel->data(studyIndex, Qt::DisplayRole).toString();

    auto* addToStudyAction = new QAction(studyName, &menu);
    QObject::connect(addToStudyAction, &QAction::triggered,
                     [=]() { this->addSelectedBaseToStudy(studyIndex); });
    studyActions.push_back(addToStudyAction);
  }
  std::sort(studyActions.begin(), studyActions.end(),
            [](QAction* p1, QAction* p2) { return p1->text() < p2->text(); });
  saveToStudies->addActions(studyActions);

  QAction* renameBase = new QAction("Rename base", this);
  renameBase->setEnabled(proxyItemIndex.isValid());
  QObject::connect(renameBase, &QAction::triggered,
                   [=]() { this->Internals->Ui.Files->edit(proxyItemIndex); });

  switch (intern.BaseModel->currentState())
  {
  case BaseDisplayState::FILTERED_BASE: {
    menu.addMenu(saveToStudies);
  }
  break;
  case BaseDisplayState::RECENT_BASE: {
    menu.addMenu(saveToStudies);
    menu.addAction(renameBase);
  }
  break;
  case BaseDisplayState::STUDY: {
    QAction* removeFromStudy = new QAction("Remove from study", this);
    removeFromStudy->setEnabled(baseIsSelected);
    QObject::connect(
        removeFromStudy, &QAction::triggered, this,
        &pqThemysAssistantMainWidget::removeSelectedBaseFromActiveStudy);

    menu.addAction(removeFromStudy);
    menu.addAction(renameBase);
  }
  break;
  default:
    break;
  }

  menu.exec(intern.Ui.Files->viewport()->mapToGlobal(menuPos));
  return;
}

//-----------------------------------------------------------------------------
void pqThemysAssistantMainWidget::fileSelectionChanged()
{
  auto& intern = *this->Internals;
  // Selection changed, update the FileName entry box
  // to reflect the current selection.
  QString fileString;
  const QModelIndexList indices =
      intern.Ui.Files->selectionModel()->selectedIndexes();
  const QModelIndexList rows =
      intern.Ui.Files->selectionModel()->selectedRows();

  if (indices.isEmpty())
  {
    return;
  }
  QStringList fileNames;

  QString name;
  for (int i = 0; i != indices.size(); ++i)
  {
    QModelIndex index = indices[i];
    if (index.column() != 0)
    {
      continue;
    }

    name = index.model()->data(index).toString();
    fileString += name;
    if (i != rows.size() - 1)
    {
      fileString += ";";
    }
    fileNames.append(name);
  }

  intern.Ui.FileName->blockSignals(true);
  intern.Ui.FileName->setText(fileString);
  intern.Ui.FileName->blockSignals(false);
}
