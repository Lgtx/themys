// From ParaView Copyright (c) Kitware Inc.
#ifndef pqThemysChangeFileNameReaction_h
#define pqThemysChangeFileNameReaction_h

#include "pqReaction.h"

/**
 * @ingroup Reactions
 * Reaction for change file of current active reader.
 */
class PQAPPLICATIONCOMPONENTS_EXPORT pqThemysChangeFileNameReaction
    : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqThemysChangeFileNameReaction(QAction* parent = nullptr);
  ~pqThemysChangeFileNameReaction() override = default;

  /**
   * Changes the input for the active source.
   */
  static void changeFileName();

public Q_SLOTS: // NOLINT(readability-redundant-access-specifiers)
  /**
   * Updates the enabled state. Applications need not explicitly call
   * this.
   */
  void updateEnableState() override;

protected:
  /**
   * Called when the action is triggered.
   */
  void onTriggered() override
  {
    pqThemysChangeFileNameReaction::changeFileName();
  }

private:
  Q_DISABLE_COPY(pqThemysChangeFileNameReaction)
};

#endif
