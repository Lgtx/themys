#include <array>
#include <string>

#include <vtkCellData.h>
#include <vtkCompositeDataSet.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkFieldData.h>
#include <vtkHyperTreeGrid.h>
#include <vtkHyperTreeGridNonOrientedGeometryCursor.h>
#include <vtkIdTypeArray.h>
#include <vtkInformation.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkMultiPieceDataSet.h>
#include <vtkNew.h>
#include <vtkPolyData.h>
#include <vtkRandomHyperTreeGridSource.h>
#include <vtkStringArray.h>
#include <vtkVersion.h>
#include <vtkXMLMultiBlockDataWriter.h>

// -----------------------------------------------------------------------------
// Global base definition
const std::string simuAuthor = "Kitware";
const std::string timestepFolderName = "times";
const std::array<double, 3> timesteps = {0.3333, 0.6666, 0.9999};
const std::array<std::string, 2> materials = {"Material1", "Material2"};
const std::array<std::string, 3> domains = {"Domain1", "Domain2", "Domain3"};
const std::array<std::array<std::array<double, 6>, 3>, 2> bounds = {
    {{{{-1, 0, 0, 1, -1, 1}, {-1, 0, -1, 0, -1, 0}, {-1, 0, -1, 0, 0, 1}}},
     {{{0, 1, 0, 1, -1, 0}, {0, 1, 0, 1, 0, 1}, {0, 1, -1, 0, -1, 1}}}}};

//------------------------------------------------------------------------------
void GenerateCellData_rec(vtkHyperTreeGrid* tree,
                          vtkHyperTreeGridNonOrientedGeometryCursor* cursor,
                          vtkDoubleArray* volume, vtkIdTypeArray* indexes,
                          vtkStringArray* cellNbStr)
{
  // Compute relative cell volume
  // a material is half a 2x2x2 cube so its volume is 2x2
  const double materialVolume = 4.0;
  double* size = cursor->GetSize();
  double currentVol = size[0] * size[1] * size[2];
  double relativeVolume = currentVol / materialVolume;
  vtkIdType id = cursor->GetGlobalNodeIndex();

  volume->SetValue(id, relativeVolume);
  indexes->SetValue(id, id);
  cellNbStr->SetValue(id, "Cell " + std::to_string(id));

  if (!cursor->IsLeaf())
  {
    // Cursor is not at leaf, recurse to all children
    int numChildren = tree->GetNumberOfChildren();
    for (int child = 0; child < numChildren; ++child)
    {
      cursor->ToChild(child);
      // Recurse
      GenerateCellData_rec(tree, cursor, volume, indexes, cellNbStr);
      cursor->ToParent();
    }
  }
}

// -----------------------------------------------------------------------------
vtkSmartPointer<vtkHyperTreeGrid> BuildDomain(int timeIdx, int matIdx,
                                              int domainIdx, bool isTwin)
{
  std::string suffixTwin = isTwin ? "_Twin" : "";
  const auto& curBounds = bounds[matIdx][domainIdx];

  vtkNew<vtkRandomHyperTreeGridSource> htgSource;
  htgSource->SetSeed(matIdx * domainIdx);
  htgSource->SetMaxDepth(3);
  htgSource->SetDimensions(3, 3, 3);
  htgSource->SetOutputBounds(curBounds.data());
  htgSource->SetSplitFraction(0.33333);
  htgSource->Update();
  vtkSmartPointer<vtkHyperTreeGrid> htg = htgSource->GetHyperTreeGridOutput();

  htg->GetNumberOfElements(0);
  vtkDataArray* depthArray = htg->GetCellData()->GetArray("Depth");
  vtkNew<vtkDoubleArray> relativeVol;
  relativeVol->SetNumberOfValues(depthArray->GetNumberOfValues());
  relativeVol->SetName(
      std::string("RelativeCellVolume").append(suffixTwin).c_str());
  vtkNew<vtkIdTypeArray> cellIndex;
  cellIndex->SetNumberOfValues(depthArray->GetNumberOfValues());
  cellIndex->SetName(std::string("CellIndex").append(suffixTwin).c_str());
  vtkNew<vtkStringArray> cellNbStrArray;
  cellNbStrArray->SetNumberOfValues(depthArray->GetNumberOfValues());
  cellNbStrArray->SetName(std::string("CellNumber").append(suffixTwin).c_str());

  // Iterate over all hyper trees
  vtkIdType index;
  vtkHyperTreeGrid::vtkHyperTreeGridIterator it;
  htg->InitializeTreeIterator(it);
  vtkNew<vtkHyperTreeGridNonOrientedGeometryCursor> cursor;
  while (it.GetNextTree(index))
  {
    htg->InitializeNonOrientedGeometryCursor(cursor, index);
    GenerateCellData_rec(htg, cursor, relativeVol, cellIndex, cellNbStrArray);
  }

  htg->GetCellData()->AddArray(relativeVol);
  htg->GetCellData()->AddArray(cellIndex);
  htg->GetCellData()->AddArray(cellNbStrArray);

  return htg;
}

// -----------------------------------------------------------------------------
void BuildHTBase(const std::string& path, const std::string& baseName,
                 const std::string& simuName, bool isTwin = false)
{
  std::string suffixTwin = isTwin ? "_Twin" : "";

  // Prepare metadata arrays
  vtkNew<vtkStringArray> simuNameArray;
  simuNameArray->SetNumberOfValues(1);
  simuNameArray->SetValue(0, simuName);
  simuNameArray->SetName(
      std::string("Simulation Name").append(suffixTwin).c_str());
  vtkNew<vtkStringArray> authorNameArray;
  authorNameArray->SetNumberOfValues(1);
  authorNameArray->SetValue(0, simuAuthor);
  authorNameArray->SetName(std::string("Author").append(suffixTwin).c_str());
  vtkNew<vtkDoubleArray> timeValueArray;
  timeValueArray->SetNumberOfValues(1);
  timeValueArray->SetName("TimeValue");

  // For each timestep
  for (int timeIdx = 0; timeIdx < timesteps.size(); ++timeIdx)
  {
    double time = timesteps[timeIdx];
    const std::string filename =
        baseName + "_t" + std::to_string(timeIdx) + ".vtm";
    timeValueArray->SetValue(0, time);

    vtkNew<vtkMultiBlockDataSet> base;
    base->SetNumberOfBlocks(materials.size());

    // For each material
    for (int materialIdx = 0; materialIdx < materials.size(); ++materialIdx)
    {
      const std::string materialName = materials[materialIdx];
      vtkNew<vtkMultiPieceDataSet> material;
      material->SetNumberOfPieces(domains.size());

      vtkNew<vtkStringArray> materialNameArray;
      materialNameArray->SetNumberOfValues(1);
      materialNameArray->SetName(
          std::string("Material").append(suffixTwin).c_str());
      materialNameArray->SetValue(0, materialName);

      base->SetBlock(materialIdx, material);
      base->GetMetaData(materialIdx)
          ->Set(vtkCompositeDataSet::NAME(), materialName);

      // For each calculation domain
      for (int domainIdx = 0; domainIdx < domains.size(); domainIdx++)
      {
        const std::string domainName = domains[domainIdx];
        vtkSmartPointer<vtkHyperTreeGrid> domain =
            BuildDomain(timeIdx, materialIdx, domainIdx, isTwin);
        domain->GetFieldData()->AddArray(simuNameArray);
        domain->GetFieldData()->AddArray(authorNameArray);
        domain->GetFieldData()->AddArray(timeValueArray);
        domain->GetFieldData()->AddArray(materialNameArray);

        material->SetPiece(domainIdx, domain);
        material->GetMetaData(domainIdx)->Set(vtkCompositeDataSet::NAME(),
                                              domainName);
      }
    }

    // Write curret base
    vtkNew<vtkXMLMultiBlockDataWriter> writer;
    writer->SetFileName(std::string(path + "/" + filename).c_str());
    writer->SetInputData(base);
    writer->SetCompressorTypeToNone();
    writer->SetDataModeToAscii();
    writer->Write();
  }
}

// -----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cout << "Path to output directory should be specified" << std::endl;
    return 1;
  }

  std::string path = argv[1];

  BuildHTBase(path, "HTBase", "HT Base Example", false);
  BuildHTBase(path, "HTBase_Twin", "HT Base Example (Twin)", true);

  return EXIT_SUCCESS;
}
