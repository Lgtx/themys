cmake_minimum_required(VERSION 3.8)

file(STRINGS Version.txt ThemysVersion)
project(Themys VERSION ${ThemysVersion})

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake)

find_package(ParaView REQUIRED)

if(NOT PARAVIEW_USE_QT)
  message(
    ERROR
    ": Please make sure ParaView has been built with PARAVIEW_USE_QT enabled.")
  return()
endif()

if(NOT PARAVIEW_USE_PYTHON)
  message(
    ERROR
    ": Please make sure ParaView has been built with PARAVIEW_USE_PYTHON enabled."
  )
  return()
endif()

option(USE_SERVERPLUGINS
       "Use the server plugins (the submodule should have been checked out)" ON)
option(BUILD_DOCUMENTATION "Build the documentation web site" OFF)

if(CMAKE_BUILD_TYPE MATCHES "^[Dd]ebug")
  include(FetchContent)
  FetchContent_Declare(
    common_tools
    GIT_REPOSITORY https://gitlab.com/themys/themyscommontools.git
    GIT_TAG main)
  FetchContent_MakeAvailable(common_tools)
  # To have access to fetched CMake scripts
  list(APPEND CMAKE_MODULE_PATH "${common_tools_SOURCE_DIR}/cmake")

  include(ClangTidy)
  include(CppCheck)
  include(CodeCoverage)
  # To generate the compilation database used by Clang tools
  set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
endif()

include(CTest)
include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY
    "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY
    "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY
    "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")

set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

add_subdirectory(plugins)
add_subdirectory(client)
if(${BUILD_TESTING})
  add_subdirectory(testing)
endif()

set(DOCUMENTATION_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/documentation)
file(RELATIVE_PATH DOCUMENTATION_RELATIVE_PATH ${CMAKE_INSTALL_PREFIX}/bin
     ${DOCUMENTATION_INSTALL_DIR})
# The generation of .h file is mandatory even the doc is not generated otherwise
# compilation fails
configure_file(${CMAKE_SOURCE_DIR}/client/pqThemysDocumentationLocation.h.in
               ${CMAKE_SOURCE_DIR}/client/pqThemysDocumentationLocation.h @ONLY)

if(${BUILD_DOCUMENTATION})
  add_subdirectory(doc)
endif()
