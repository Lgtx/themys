# Special Thanks

We are sincerely grateful to the following persons whose work, as members of the [LOVE project](https://www-hpc.cea.fr/fr/Opensources.html), was the base of our inspiration for Themys:

- Daniel Aguilera
- Olivier Bressand
- Thierry Carrard
- Jérôme Dubois
- Agnès Grenouille
- Claire Guilbaud
- Guénolé Harel
- Jacques-Bernard Lekien
- Jérôme Schneider
