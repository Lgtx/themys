Documentation: `doc en <../en/index.html>`__

Bienvenue dans la documentation de Themys !
===========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   history/history
   gettingstarted/gettingstarted
   tutos/tutos
   procedures/procedures
   readers/readers
   scripting/scripting
   filters/filters
   writers/writers

Sommaire
========

* :ref:`search`
