set(DOC_FILES_FR
    ${DOC_FILES_FR}
    ${CMAKE_CURRENT_LIST_DIR}/AnimationScreenshot.rst
    ${CMAKE_CURRENT_LIST_DIR}/ParaViewPythonScripting.rst
    ${CMAKE_CURRENT_LIST_DIR}/ScriptExecution.rst
    ${CMAKE_CURRENT_LIST_DIR}/scripting.rst
    PARENT_SCOPE)
