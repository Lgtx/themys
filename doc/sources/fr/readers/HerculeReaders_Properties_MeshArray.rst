Documentation: `doc en <../../en/readers/HerculeReaders_Properties_MeshArray.html>`__


Mesh Array
^^^^^^^^^^

La propriété **Mesh Array** liste les différents maillages de simulation qui ont été écrits par le
code de simulation. L'utilisateur peut ainsi sélectionner les maillages de simulation
qui lui semblent d'intérêt pour l'analyse à réaliser.

Ces maillages peuvent représenter différents aspects de la simulation allant du maillage
*d'éléments finis classique* de la simulation, de plans de coupe, de surfaces, de courbes, de particules
ou encore de rayons.

Contrairement à LOVE, aucune activation n'est nécessaire pour voir apparaître des contenus cachés de la base.
Dans Themys, il a été décidé de restituer l'intégralité du contenu de la base.

L'utilisateur devra ensuite sélectionner les
`matériaux <HerculeReaders_Properties_MaterialArray.html>`_ puis les `champs de valeurs <HerculeReaders_Properties_DataArray.html>`_.
Bien entendu, ces choix devront être cohérents. Ce n'est qu'à l'issue de toutes ces sélections
que l'utilisateur pourra confirmer cet choix et déclencher le chargement en cliquant sur **Apply**.

A tout moment, l'utilisateur peut revenir sur ses choix.

.. warning::
   Il est conseillé de sélectionner le juste nécessaire car c'est ce qui sera chargé lors d'un changement de
   temps de simulation comme lors d'un parcours temporel pour les services proposés par la **GUI Themys**
   de type *over times*.
