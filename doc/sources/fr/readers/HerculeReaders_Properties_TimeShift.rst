Documentation: `doc en <../../en/readers/HerculeReaders_Properties_TimeShift.html>`__


Time Shift
^^^^^^^^^^

Cette propriété offre la possibilité d'appliquer un décalage (valeur positive ou négative) au temps demandé
à travers la **GUI Themys**. Cela peut être pratique dans la comparaison de deux simulations n'ayant pas opté pour
la même origine temporelle.

La valeur de cette propriété est tout simplement ajoutée au temps de simulation demandé par la **GUI Themys**.
C'est ce temps qui est alors utilisé en interne au lecteur et sur lequel on applique alors le modus operanti décrit
dans `Current Time <HerculeReaders_Properties_CurrentTime.html>`_ (précisément le temps demandé, ou le temps
le plus proche par en-dessous ou à défaut le temps le plus proche par au-dessus).

Le positionnement de cette propriété à 0 revient à la désactiver.

Cette propriété est ignorée si un temps a été fixé par la propriété
`Fixed Time <HerculeReaders_Properties_FixedTime.html>`_.
