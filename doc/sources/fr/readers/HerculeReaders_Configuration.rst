Documentation: `doc en <../../en/readers/HerculeReaders_Configuration.html>`__


Configuration des propriétés des lecteurs Hercule
-------------------------------------------------

Il est possible de prédéfinir des listes d'objets à sélectionner par défaut.

Cette configuration est décrite dans le fichier utilisateur ``~/.config/CEA/themys-UserSettings.json``
de la façon suivante :

| {
|   "HerculeReader":
|   {
|     "DefaultSelectedMeshes" : ["meshNS", "meshNSYZ"],
|     "DefaultSelectedMaterials" : ["MatB", "global_meshNSYZ"],
|     "DefaultSelectedNodeFields" : ["Noeud:node_densite"],
|     "DefaultSelectedCellFields" : ["Milieu:mil_densite", "Maillage:cell_vecteurA1"]
|   }
| }

La description de cette configuration est suffisamment lisible pour ne pas nécessiter plus de détail.

Lors du chargement d'une base, si, pour chacune des listes, aucune valeur par défaut n'est spécifiée alors
le lecteur sélectionnera :

- le premier maillage par ordre alphabétique,
- tous les matériaux sauf ceux préfixés par *global_* ;
- **vtkPointId** pour les champs de valeurs aux noeuds ;
- **vtkCellId** et **vtkDomainId** pour les champs de valeurs aux cellules.
