Documentation: `doc en <../../en/readers/HerculeReaders_Properties_MemoryEfficient.html>`__


Memory efficient
^^^^^^^^^^^^^^^^

Cette propriété permet de réaliser un gain mémoire d'un facteur 2 lors du chargement d'un champ
de valeurs flottantes en transformant son type informatique de *double* à *float*.
