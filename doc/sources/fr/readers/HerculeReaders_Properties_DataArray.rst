Documentation: `doc en <../../en/readers/HerculeReaders_Properties_DataArray.html>`__


Cell and Point Data Array
^^^^^^^^^^^^^^^^^^^^^^^^^

Les propriétés **Cell Data Array** et **Point Data Array** listent les différents champs de valeurs disponibles
sur les différents matériaux présents dans les différents maillages de simulation.
L'utilisateur peut ainsi sélectionner les champs de valeurs de la simulation qui lui semblent d'intérêt
pour l'analyse à réaliser.

Dans le cas du lecteur **Hercule Services Reader**, les champs de valeurs préfixés par :

* **Maillage** sont liés aux matériaux préfixés par **global_** ;
* **Milieu** sont liés aux matériaux non préfixés.

Ainsi, le fait de sélectionner **Material:Density** a pour effet de charger tous les champs de
valeurs **Density** présents au niveau des matériaux sélectionnés.

Dans le cas du lecteur **Hercule HIc Reader**, les champs de valeurs suffixés par **(global)** sont liés
aux matériaux préfixés par **global_**.
Les champs de valeurs non préfixés sont liés aux matériaux non préfixés.

Cela se complique quelque peut lorsqu'on introduit l'aspect **Constituant** et **Fluide** dont les noms des
champs seront forcément préfixés par le nom du constituant ou fluide qui est un des composants d'un matériau,
ceci afin de les différencier entre eux. Si chacun des matériaux a des constituants et fluides différents
cela multiplie d'autant la liste des champs de valeurs proposés.

Généralement, cette sélection est à faire après les sélections des
`maillages <HerculeReaders_Properties_MeshArray.html>`_ et des `materiaux <HerculeReaders_Properties_MaterialArray.html>`_.
Bien entendu, ces choix devront être cohérents. Ce n'est qu'à l'issue de toutes ces sélections que
l'utilisateur pourra confirmer ce choix et déclencher le chargement en cliquant sur **Apply**.

A tout moment, l'utilisateur peut revenir sur ses choix.

.. warning::
   Il est conseillé de sélectionner le juste nécessaire car c'est ce qui sera chargé lors d'un changement de
   temps de simulation comme lors d'un parcours temporel pour les services proposés par la **GUI Themys**
   de type *over times*.
