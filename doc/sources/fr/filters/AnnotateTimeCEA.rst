Documentation: `doc en <../../en/filters/AnnotateTimeCEA.html>`__

Filtre Annotate Time CEA
========================

Ce filtre affiche, dans la fenêtre de rendu, la valeur du temps ainsi que l'indice du pas de temps auxquels la bande est chargée.

Seule la propriété **Expression** est disponible pour ce filtre. Sa valeur par défaut est:

``'Temps %f $\mu$s (#%d)' % (vtkFixedTimeValue*1e6, vtkFixedTimeStep)``

Ici, le temps est considéré comme étant exprimé en micro-seconde (1e-6 seconde).

.. Commentaire pour développeurs:
.. Si la lettre grecque correspondant à l'ordre de cette unité n'est pas correctement affichée, c'est qu'il y a
.. eu un problème d'accès ou d'installation du module matplotlib du Python 3 utilisé par **Themys**.
