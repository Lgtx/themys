Filtre Ghost Cells Generator
============================

Description
-----------

Lors d’un dépouillement en parallèle, l’application du filtre
``GhostCellsGenerator`` permet d’appliquer correctement les filtres
nécessitant une information de voisinage.

Cela concerne tout particulièrement les filtres :

-  de rendu “silhouette” (``FeaturesEdges``) ou rendu surfacique avec
   une transparence ;

-  de calcul de ``Gradient`` ;

-  de génération d’\ ``IsoContour`` ;

Mise en garde
-------------

**Sans l’application de ce filtre, le résultat obtenu est incorrect** et
dépend de la distribution du maillage sur les différents serveurs de
dépouillement :

-  rendu “silhouette” (``FeaturesEdges``) avec des “silhouettes”
   supplémentaires internes (voile artificiel) qui apparaissent ;

-  rendu surfacique avec une transparence qui révélent des surfaces
   supplémentaires internes (voile artificiel) ;

-  calcul de ``Gradient`` incorrect aux bords de chaque partie gérée par
   un serveur de dépouillement ;

-  génération d’\ ``IsoContour`` incorrect aux bords de chaque partie
   gérée par un serveur de dépouillement.

Pourquoi ne pas toujours appliquer ce filtre ?
----------------------------------------------

L’application de ce filtre est coûteux en temps elapse comme en mémoire.
Il est inadapté de l’appliquer s’il n’apporte pas quelque chose
immédiatement dans le pipeline.

Une démarche peut être la suivante : au lieu de calculer un ``Gradient``
sur tout le maillage, le faire sur une partie du maillage :

-  extraire une partie du maillage ; puis

-  appliquer un filtre de génération de cellules fantômes sur cette
   partie ; ensuite

-  appliquer le filtre de gradient.

Le résultat obtenu sur les cellules aux bords étant fausse, il reste
alors à les retirer.
