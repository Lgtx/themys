Documentation: `doc en <../../en/filters/HyperTreeGridFragmentation.html>`__

Filtre HyperTreeGridFragmentation
=================================

Description
-----------

Le filtre de Fragmentation identifie et carat-rise les fragments/régions dans un maillage Hyper Tree Grid.

Un fragment est décrit par un groupe de cellules voisines (partageant des noeuds) actives (non masquées).

Dans les faits, les fragments sont sépérés par des cellules feuilles masquées ou non définies.

Options
-------

Options afin de récupérer des champs aux cellules suivant une sémantique identifiée :

* ``EnableMassName`` et ``MassName`` permettent de déclarer un champ aux cellules décrivant une masse ;
* ``EnableDensityName`` et ``DensityName`` permettent de déclarer un champ aux cellules décrivant une densité/masse volumique ;
* ``EnableVelocityName`` et ``VelocityName`` permettent de déclarer un champ aux cellule décrivant une  vitesse ;

D'autres options :

* ``ExtractEdge`` permet de n'extraire que les centres des cellules qui composent un fragment/région
  et qui sont au bord ;
  une cellule de côté comporte au moins une cellule voisine masquée ou manquante. En exécution distribuée
  (utilisation de plusieurs serveurs distants ou non), l'application de cette option nécessite d'avoir
  appliquer auparavant le filtre ``GhostCellGenerator``.

En sortie
---------

En fonction des options enregistrés, certains champs globaux des fragments/régions seront
disponibles ou non en sortie.

Une version de chacun des champs sera un ``FieldData`` lorsque le fragment est décrit par
un ``PolyData`` qui décrit une liste de points correspondants à des cellules du fragment
(toutes les cellules ou seulement celles du bord avec l'option ``ExtractEdge``).

Voici la dénomination et signification sémantique de ces champs que l'on peut retrouver en sortie :

  * ``FragmentId`` est l'index du fragment qui a ce centre ;
  * ``Pounds`` est le nombre de cellules qui compose ce fragment ;
  * ``Volume`` est le volume du fragment ; ce calcul ne prend pas
    en compte l'aspect mixte d'une cellule via
    la description d'une interface ou d'une fraction volumique ;
  * ``Mass`` (uniquement si ``MassName`` ou ``DensityName`` a été renseigné) est la masse du fragment ;
  * ``Density`` (même condition que ``Mass``) est la densité, c'est à dire le quotient de la masse par le volume par cellule ;
  * ``AvgVelovity`` (uniquement si ``VelocityName`` a été renseigné) est la vitesse moyenne du fragment ;
  * ``Velocity`` (même condition que ``AvgVelovity``) est la vitesse moyenne pondérée par la masse : la somme du produit de la vitesse
    par la masse par cellule divisée par la masse globale du fragment ;

Version 0.2
-----------

Développé initialement en 2022.
Cette version 0.2 résout les problèmes relatifs à la version 0.1 (au moins les Issues #1 to #4 du projet isolé)
en mettant de côté l'aspect exécution distribué, entre autre.

En entrée
~~~~~~~~~

Avec la version 0.2, l'entrée a un peu changé.

Empêcher la sortie de la description du contour de chacun des fragments est toujours présent en option.

Associer une grandeur portée par les mailles à une sémantique est une nouvelle possibilité.

Ces quantités sont :

    * ``Mass``: la masse de chaque cellule ;
    * ``Density``: la masse volumique de chaque cellule ;
    * ``Velocity``: la vitesse de chaque cellule.

Une case à cocher permet de prendre en compte le champ de valeurs choisi.

Entrez le nom d'un champ de valeur qui représente la masse ou la densité pour activer la sortie de ces champs.

De même, la vitesse moyenne fait partie de la sortie si le nom d'un champ de valeur de vitesse est donné.

Associer le nom d'un champ, qui représente une masse ou une densité, à celui qui représente une vitesse,
produit un champ de vitesse pondéré en masse dans les sorties.

En sortie
~~~~~~~~~

La sortie reste inchangée entre la version 0.1 et 0.2 sauf la liste des champs de valeur et la façon
de les calculer (faux en version 0.1).

Champs de valeur encore présents :

    * pour ``Centers`` sur ``CellData``;
    * pour ``Fragment_#`` sur ``FieldData``.
      Malheureusement, actuellement `ParaView` ne permet pas de coloriser un maillage en fonction
      d'un FieldData sur un HyperTreeGrid.

Les champs de valeur attachés à un fragment sont les suivants :

    * ``FragmentId`` : numérotation continue des fragments commençant par zéro ; le nommage du bloc décrivant
      ce fragment est conforme à cette valeur (contrairement à la version 0.1.0) ;
    * ``Pounds`` : le nombre de cellules qui contribuent à ce fragment ;
    * ``Volume`` : le volume total ;
    * ``Mass`` : la masse totale ;
    * ``Density`` : la densité (c'est-à-dire la ``Mass`` divisée par le ``Volume``) ;
    * ``AvgVelocity`` : la vitesse moyenne, la somme des vitesses de chaque cellule contributive divisée par ``Pounds`` ;
    * ``Velocity`` : la vitesse en tant que quantité extensive par la masse, le quotient de la somme du produit de
       la vitesse par la masse de chaque cellule contributive par la ``Masse`` totale.

Fait
~~~~

Une procédure de test manuel (à défaut de pouvoir disposer de cette base au niveau de la CI de GitLab) valide cette version du filtre.

La séquence à réaliser est la suivante :

* charger le cas Armen avec le lecteur Hercule (``Readers``) :

    * activer l'option ``HIc API usage`` permettant le chargement d'un maillage AMR sous la description vtkHyperTreeGrid ;
    * sélectionner uniquement le matériau ``Cu1`` ;
    * sélectionner les champs de valeur ``EqPlastique``, ``Vitesse`` et ``rho`` ;

* appliquer un filtre seuil sur ``EqPlastique`` pour les valeurs [0 ; 1.1] ;

|image0|

* appliquer ce filtre de Fragmentation avec ``rho`` et ``Vitesse`` (case déjà active) avec les arêtes.

|image1|

Vous pouvez explorer dynamiquement dans un ``RenderView`` en sélectionnant l'option ``Hover Points On`` (un point
rouge surmonté d'un point d'interrogation) puis en plaçant le curseur sur un centre de fragment ou l'un des nœuds
participant au tour d'un fragment. Les informations relatives à ce fragment seront alors visibles dans une info-bulle.

Il est possible d'isoler l'affichage d'un fragment. Attention, dans la ``SpreadSheetView``, à bien sélectionner
``Attribut`` nommé ``Point Data`` lorsque vous explorez le ``Block Name`` nommé ``Centers`` et à basculer vers ``Attribute``
ommé ``FieldData`` pour les blocs décrivant le périmètre d'un fragment.

|image2|

Comme afficher le champ de vitesse moyenne à partir des centres des fragments.

|image3|


Version 0.1
-----------

Développé en 2020. Le prototype 0.1 n'a pas donné satisfaction.

En entrée
~~~~~~~~~

Output of the description of the contour of each of the fragments may be deactivated through an option.

Indisputably, it was necessary to give the name of two quantities: one which represents the density and the other a speed.

En sortie
~~~~~~~~~

Ce filtre renvoie un vtkMultiBlockDataSet :

* le premier bloc ``#0`` nommé ``Centers`` est un ``vtkPolyData`` de points,
  chaque point caractérise le centre d'un fragment (la moyenne des coordonnées des cellules
  qui définissent un fragment) auquel est associé le champ de valeurs décrivant ce fragment comme ``CellData`` ;
* les autres blocs nommés ``Fragment_#``, compris entre ``#0`` et ``#N-1`` (N le nombre de fragments), sont ``vtkPolyData``.
  Chacun de ces blocs décrit le bord d'un fragment sans ordre privilégié et est associé à des champs de valeurs décrivant
  ce fragment comme ``FieldData``.

Fait
~~~~

Il localise les centres des fragments mais avec des bords incorrects (parfois des cellules internes à un fragment
ont été conservées). De plus, les champs de valeur associés (CellData ou FieldData) n'étaient pas correctement calculés.
La version parallèle n'a jamais fonctionné. Et enfin, diverses fonctionnalités manquaient, que ce soit au niveau des champs
de valeur retournés ou de la caractérisation de la forme de chaque fragment.

.. |image0| image:: ../../img/filters/HyperTreeGridFragmentation_1.png
.. |image1| image:: ../../img/filters/HyperTreeGridFragmentation_2.png
.. |image2| image:: ../../img/filters/HyperTreeGridFragmentation_3.png
.. |image3| image:: ../../img/filters/HyperTreeGridFragmentation_4.png
