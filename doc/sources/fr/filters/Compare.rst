Filtre Compare
==============

Description
-----------

Le filtre **Compare** permet à tout moment de comparer deux bases.

Il réalise 3 actions :

# la fusion des temps de simulation dans la partie **Time parameters** du filtre ;

# l'application d’une symétrie planaire sur la seconde base ;

# le renomage automatique des noms des éléments attachés aux bases.

La notion de *premier* et *second* base dépend de l’ordre dans lequel ont été sélectionnées les bases à comparer
dans l'explorateur du pipeline (**Pipeline Browser**).

.. note::
   Seules les deux premières bases sélectionnées sont utilisées par ce filtre. Les autres bases sont tout
   simplement ignorées.

Propriétés
----------

|image0|

Ici, on retrouve les différentes propriétés qui sont proposées.

Time parameters
^^^^^^^^^^^^^^^

L’utilisateur peut définir une valeur de tolérance temporelle, absolue (par défaut) ou relative,
en dessous de laquelle deux temps de simulation (**Timestep**) des deux bases peuvent êtres considérées
comme identiques et être fusionnées dans le résultat proposé.

Le passage en tolérance relative se fait en cochant l'option **Use Relative Tolerance**.

L'utilisateur peut également choisir de ne conserver que les temps de simulation communs entre les deux
bases en cochant l’option **Intersection**, incluant nécessaire la prise en compte de La tolérance.

Reflection parameters
^^^^^^^^^^^^^^^^^^^^^

Les paramètres décrivant la symétrie peuvent être définis en tant que champs de valeurs globaux (**FieldData**)
au niveau de cette seconde base.
Les noms de ces **FieldData** sont **Plane**, avec une valeur compris dans [0, 8], et **DistanceToOrigin**,
tous les deux en valeurs *double*.

.. note::
   Rappelons que les champs de valeurs globaux doivent être définis au niveau de chaque maillage VTK
   et ne peuvent pas être tenu par un **vtkMultiBlockDataSet** ni un **vtkMultiPieceDataSet**.

L'attribution des paramètres de la symétrie se fait, par défaut, en exploitant ces champs de valeurs
globaux. S'ils n'existent pas, par défaut, **Plane** sera positionné à 0 (Xmin) et **DistanceToOrigin** à 0,
la valeur de cette dernière n'ayant pas d'importance suivant l'option retenue pour le plan de coupe.

Par la suite et à tout moment, l'utilisateur peut définir ses propres valeurs décrivant cette symétrie
en cochant l'option **use Entered Values**.

Deux paramètres influent alors sur le positionnement du plan de symétrie qui sera appliqué à cette seconde base :

# **Plane** permet de définir le plan utilisé pour la symétrie. En considérant les valeurs min et max
  comme étant les valeurs aux abords de la boîte englobante de la base subissant la symétrie, ce paramètre
  peut prendre les valeurs suivantes :

  # Xmin (0) ;
  # Ymin (1) ;
  # Zmin (2) ;
  # Xmax (3) ;
  # Ymax (4) ;
  # Zmax (5) ;
  # X Distance to Origin (6) ;
  # Y Distance to Origin (7) ; et
  # Z Distance to Origin (8).

# **Distance To Origin** permet de définir la distance à l’origine du plan, dans le cas où le
  paramètre **Plane** prends la valeur *X Distance to Origin*, *Y Distance to Origin* ou *Z Distance to Origin*.

Paramètres de renommage
^^^^^^^^^^^^^^^^^^^^^^^

Dans le menu **Settings** onglet **Themys**, il est possible d'unifier les nommages d'éléments venant de
différentes bases.

|image1|

Ici, un exemple de la section **Matching table for array renaming** qui permet durablement de faire l'association
entre un ou plusieurs nommages présents dans différentes bases (à droite, ici *vtkInterfaceFraction*
et *Milieu:fracpres*) et un nommage pour la visualisation (à gauche, ici *frac*).

L'interface est suffisemment intuitive pour ajouter, supprimer ou modifier une association.

La création d'une nouvelle association nécessite :

# d'écrire à gauche, un *nouveau nom* ;

# d'écrire à droite la liste des *anciens noms* qui seront remplacés dans l'interface par le *nouveau nom*.
  La saisie de cette liste de nouveaux noms se fait en les séparant par des points virgules.

.. note::
   A charge à l'utilisateur d'attribuer un nommage différent de ce que l'on peut trouver dans les bases.

Connaissance avancée
--------------------

Ce filtre est en fait une composition de filtres que vous pouvez utiliser indépendemment.

Gestion des temps
^^^^^^^^^^^^^^^^^

**Themys** aggrége par défaut les listes des temps de simulation issues des bases ouvertes.
La liste produite au niveau de la GUI disponible dans la barre d'outils de contrôle du temps
est une union exacte ordonnée de tous les pas de temps.
Lorsque l'utilisateur demande un temps de simulation à travers cette barre d'outils, la valeur de ce
temps est transmis à chaque base ouverte afin, en fonction du mécanisme propre à chaque base, il charge
le temps demandé. Généralement, si le temps demandé n'existe pas dans la base, il est chargé le temps
le plus proche par en-dessous, puis par au-dessus si ce dernier n'existe pas.

Chaque base ne connaît que ses propres temps de simulation, mais l'application **Themys**
en propose l'union dans la GUI.

Cette liste est facilement pilotable avec la **vue** (**View**) **Time Inspector** qui permet de prendre
en compte ou non la liste des temps proposée par une base en sélectionnant les bases dans cette vue.

Le filtre **Merge Time** permet aussi de construire une nouvelle liste de temps à partir de plusieurs,
afin d'unifier les temps venant d'une ou plusieurs bases ; on en retrouve d'ailleurs les propriétés
dans le filtre **Compare** qui l'utilise en interne.

Le filtre **Synchronize Time** remplace chaque valeur des temps de simulation d'une base
(**Data to Pass**) par la valeur la plus proche à l'intérieur d'une tolérance relative à une seconde
base (**Time to Sync**). La tolérance spécifiée dans ce filtre est un facteur appliqué à la plage
de temps. Pour être précis, le pas de temps d'indice *i* dans la liste *t1* est remplacé par *t2* si :
``abs(t1[i] - t2) < (Tolerance * abs(t1[last] - t1[0])``.

Le filtre **Force Time** permet de positionner la valeur d'un temps de simulation dans le pipeline
permettant ainsi d'ignorer le temps positionné au niveau de la GUI. Cette valeur est saisie sans assurance
de tomber exactement sur un temps de simulation de la base (dans ce cas, le mécanisme d'attribution
d'un temps de simulation est appliqué au niveau du lecteur de données concerné).
Dans le cas du lecteur Hercule, nous conseillons vivement d'utiliser la propriété **Fixed Time** qui
offre la possibilité de choisir un temps parmi ceux disponible dans la base.

Comparaison de valeurs entre bases
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Dans le cas de bases ayant la même géométrie, il est possible de transférer tous les champs
aux points et aux cellules de la première base afin de produire une sortie unique contenant
tous les champs de valeurs venant des bases.
Les bases ayant un nombre de points et de cellules différent de la première base sont automatiquement
ignorées.
Pour pouvoir appliquer le filtre **Append Attributes** correspondant à cela, il est nécessaire
d'avoir grouper (**Group Datasets**) toutes les bases concernées afin de former une collection.

.. |image0| image:: ../../img/filters/compare_filter.png
.. |image1| image:: ../../img/filters/settings_compare_filter.png
