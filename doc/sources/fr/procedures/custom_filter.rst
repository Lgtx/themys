Documentation: `doc en <../../en/procedures/custom_filter.html>`__



Filtre customisé
================

Description
-----------

Dans le cadre d’un dépouillement incluant souvent une séquence de
filtres, l’utilisateur a la possibilité de définir un filtre customisé
décrivant une séquence de filtres et n’exposer que les propriétés qui
sont pertinentes.

**Themys** offre cela à travers la création d’un filtre customisé
(**Custom Filter**) dont l’utilisateur décide du nom mais aussi dont il
précise les entrèes (**inputs**), les sorties (**outputs**) ainsi que les
propriétés (**properties**) qu'il souhaite exposer.

Procédure de création
---------------------

La procèdure pour construire un filtre customisé se fait en 6 étapes.

1. Construire un pipeline fonctionnel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Afin de construire un filtre customisé, il faut avoir déjà construit
un pipeline fonctionnel visible dans le navigateur du pipeline
(**Pipeline Browser**).

Pour l’exemple, nous avons défini l’enchainement de filtres suivant :

* une source ici la lecture d’une base ;

* application de deux filtres **Threshold** sur cette même source avec
  des valeurs de seuil différentes ;

* application d’un **Group Datasets** sur les sorties des *threshold*.

|image0|

2. Sélection d’une partie continue du pipeline
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour l’exemple, nous avons sélectionné ici une partie du pipeline
qui reprend le pipeline sans la source correspond à la lecture d'une base.

|image1|

Dans la suite de l’exemple, nous incluerons la source.

3. Créer le filtre customisé
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Comme indiqué sur l’image précédente, une fois la sélection de
l’enchaînement des filtres fait, la création d’un filtre customisé se
fait :

*  soit en cliquant sur le bouton droit de la souris au niveau de la
   sélection pour cliquer sur le menu **Create Custom Filter** ;

*  soit par le menu **Tools** > **Create Custom Filter**.

Une boîte de dialogue s’ouvre alors.

4. Nommer le nouveau filtre customisé
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vous devez alors saisir le nom de votre nouveau filtre customisé, pour
l’exemple ce sera *StHelensMBDS2* (StHelens étant la source et un output
de type Mutli-Block Data Set comprend deux blocs qui sont les résultats
des deux *Threshold*), puis cliquer sur le bouton **Next**.

.. warning::
   Il est interdit de donner un nom de filtre déjà existant.

Plus loin, nous vous indiquerons comment supprimer un filtre customisé.

|image2|

5. Choisir les entrées/*inputs*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cela se fait de la même façon que pour les sorties/*outputs* que
l’on verra à l’étape suivante.

Sur un pipeline linéaire enchaînant des filtres l’un derrière l’autre,
il est généralement proposé l’*input* du premier filtre.

Contrairement à l’image de l’étape 2, nous avons opté pour l’exemple de
prendre la source, de fait, nous n’avons pas d’entrée à définir. Nous
pouvons juste cliquer sur le bouton **Next**.

|image3|

5. Choisir les sorties/*outputs*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sur un pipeline linéaire enchaînant des filtres l’un derrière l’autre,
il est généralement proposé l’*output* du dernier filtre.

Ici, l’utilisation d’un **Group Datasets** semble rentre cette tâche
d’optimisation un peu compliqué (pour lui, il existe plusieurs sorties,
celles des **Threshold** et celle du **Group Datasets**).

Il est alors nécessaire que vous précisiez l’*output* souhaité (voir
plusieurs, si c’est votre souhait).

|image4|

Il suffit alors de sélectioner un des filtres de la partie du pipeline
retenue pour le filtre customisé que l’on retrouve à gauche dans la
boîte de dialogue pour que cela entraîne la mise à jour de la liste des
**Output Port**.

Vous pouvez alors choisir un des **Output Port** qui généralement
définit un nom dans **Output Name** vous pouvez ensuite le personnalisé si
vous le souhaitez. Pour finir, vous devez cliquer sur le bouton **+** pour ajouter
cet *output*.

L’ajout d’un second *output* se fait de la même façon, en ayant la
possibilité de choisir un autre des filtres du pipeline.

Une fois cette étape réalisée vous devez cliquer sur **Next**.

6. Choisir les propriétés
~~~~~~~~~~~~~~~~~~~~~~~~~

Le choix des propriétés se fait de façon similaire à la procédure
décrite dans l’étape 5.

On peut décider de ne pas exposer de propriétés.

Par exemple, on peut décider d’exposer la propriété de la source
**File Name** afin de pouvoir changer le nom de la base à ouvrir.

|image5|

On finalise cette étape ainsi que la création du filtre customisé en
cliquant sur **Finish**.

.. note::

   A tout moment de ces étapes vous pouvez revenir sur une
   étape précédente tant que vous n’avez pas cliqué sur **Finish**.

.. warning::

   Il n’est plus possible de modifier un filtre customisé une fois
   que vous avez cliqué sur **Finish**.
   Il vous reste néanmoins la possibilité de l’exporter pour le
   partager... ou le détruire (voir plus loin).

Lister les filtres customisés
-----------------------------

Vous pouvez vérifier ensuite que le nouveau filtre customisé est bien
disponible parmi les **Sources** ou les **Filtres**.

Comme ici, notre filtre comprend un filtre **Source** (le lecteur sur la
base *StHelens*), le filtre customisé est dont un filtre **Source**.
C’est ce que l’on constate pour cet exemple.

|image6|

Gérer les filtres customisés
----------------------------

La gestion des filtres customisés se fait en passant par le menu
**Tools** > **Manage Custom Filters**.

|image7|

Cela ouvre une boîte de dialogue où l’on peut importer un nouveau filtre
customisé. Après sélection d’un filtre customisé à gauche, on peut aussi
l’exporter ou le détruire. Il n’est pas possible de modifier un filtre
customisé.

Lancement du nouveau filtre customisé
-------------------------------------

A la façon des autres filtres, on lance le nouveau filtre customisé.
On constate que pour cet exemple, la propriété **File Name** est bien exposé.
Comme pour tous les filtres, le filtre sera appliqué une fois que l’on a cliqué sur **Apply**.

|image8|

A remarquer que ce qui est exposé dans le pipeline n’est que le nom du
filtre customisé et non pas sa description interne compléte.

.. |image0| image:: ../../img/procedures/CustomFilter_1.png
.. |image1| image:: ../../img/procedures/CustomFilter_2.png
.. |image2| image:: ../../img/procedures/CustomFilter_3.png
.. |image3| image:: ../../img/procedures/CustomFilter_4.png
.. |image4| image:: ../../img/procedures/CustomFilter_5.png
.. |image5| image:: ../../img/procedures/CustomFilter_6.png
.. |image6| image:: ../../img/procedures/CustomFilter_7.png
.. |image7| image:: ../../img/procedures/CustomFilter_8.png
.. |image8| image:: ../../img/procedures/CustomFilter_9.png
