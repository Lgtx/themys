Documentation: `doc en <../../en/procedures/contouring.html>`__



Iso-surfaces et iso-lines
=========================

Description
-----------

L'opération d'extraction d'une iso-surface ou d'une iso-ligne fait l'objet d'extraire
une surface ou une ligne qui aurait la même valeur scalaire dans le domaine de simulation.

Des exemples bien connus des iso-lignes sont ceux utilisés :

* en cartographie pour indiquer les lignes de niveau, une ligne décrit les lieux ayant la même altitude,

* en météorologie pour indiquer les lignes de pression ou de température, une ligne décrit les lieux ayant la même pression ou température.

Les filtres répondant à cette fonctionnalité permettent de produire plusieurs extractions en même temps.
La modification de l'opacité, qui est l'inverse de la transparence, sur l'objet résultant affectera
toutes les surfaces/lignes ainsi produites.

Le cas de l'utilisateur souhaitant appliquer une opacité différente par surface/ligne sera abordé.

Via l'interface graphique
-------------------------

L'application du filtre **Contour** nécessite de sélectionner une des
instances d'objets/sources du navigateur du pipeline (**Pipeline Browser**)
en cliquant dessus avant de créer une instance de ce filtre **Clip**.
Cette création peut se faire en cliquant sur un bouton.

|image0|

Sélectionnez le tableau scalaire qui servira à l'extraction d'un ou des contours à l'aide
de la liste déroulante **Contour by** (contour par) dans le panneau **Properties** (propriétés).
Par défaut, la valeur proposée pour l'extraction est fixe à la valeur moyenne de la plage
de données scalaires, visible dans une liste dans la section **Isosurfaces**.

|image1|

Cette liste peut contenir autant de valeurs que vous le souhaitez, chaque valeur
produira alors un contour.
Diverses options sont proposées pour ajouter ou supprimer des valeurs, elle sont
détaillées ci-dessous.

|image2|

* le bouton **+**
  Ajoute une nouvelle valeur à la liste sous la valeur actuellement sélectionnée.

* le bouton **-**
  Supprime la valeur sélectionnée dans la liste.

* le bouton de génération automatique de valeurs (**Automatic values generator button**)
  Ouvre une fenêtre proposant des options pour générer des valeurs de contour distribuées de
  différentes façons (linéaire, logarithmique, etc) dans une plage donnée.

  |image3|

* le bouton de suppression des valeurs (**Delete button**)
  Supprime toutes les valeurs de la liste.

* le bouton d'échelle (**Scaling button**)
  Divise ou multiplie toutes les valeurs par 2.

* le bouton de réinitialisation (**Reset button**)
  Réinitialise la liste à son état d'origine.

Pour modifier une valeur de la liste, il suffit de cliquer dessus et de saisir une
nouvelle valeur.

1. Modification commune de l'opacité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour positionner le même niveau d'opacité à toutes les iso-surfaces/lignes produites à
l'issue de l'application du filtre **Contour**, il suffit d'ajuster la valeur de
l'opacité (**Opacity**) dans la section de l'affichage (**Display**) comme indiqué ci-dessous.

|image4|

2. Modification ciblée de l'opacité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour obtenir différents niveaux d'opacité pour chaque iso-surface/ligne produite,
commencez par ouvrir l'éditeur de la map de couleurs (**Color Map Editor**) dans la
barre d'outils comme indiqué ci-dessous.

|image5|

Cochez l'option qui active le mappage d'opacité pour les surfaces
(**Enable Opacity Mapping For Surfaces**) pour en modifier le mappage d'opacité.
La courbe noire superposée à la carte des couleurs indique le niveau d'opacité
associé à la couleur correspondante (valeur scalaire).
L'image ci-dessous illustre la courbe d'opacité par défaut, où la valeur
maximale en rouge est complètement opaque, tandis que la valeur minimale
en bleu est complètement transparente.

|image6|

Cliquez à l'intérieur du graphique de couleur et d'opacité pour ajouter de
nouvelles valeurs d'opacité à la courbe. Les points existants peuvent ensuite
être déplacés pour ajuster leurs valeurs.

|image7|

Via le scripting Python
-----------------------

.. code-block:: python

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set to contour
   mySource = FindSource('Wavelet1')

   # Create a new 'Contour' filter
   contour = Contour(Input=mySource)

   # If needed, change scalar data array
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'RTData' with a valid scalar array
   contour.ContourBy = ['POINTS', 'RTData']

   # Define as many isocontour values as needed
   contour.Isosurfaces = [90.0, 150.0]

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   contourDisplay = Show(contour, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   contourDisplay.SetScalarBarVisibility(renderView, True)

1. Modification commune de l'opacité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

   # Set common opacity
   contourDisplay.Opacity = 0.5

2. Modification ciblée de l'opacité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

   # Get color map for 'RTData'
   colorMap = GetColorTransferFunction('RTData')

   # Enable opacity mapping
   colorMap.EnableOpacityMapping = 1

   # Get opacity map for 'RTData'
   opacityMap = GetOpacityTransferFunction('RTData')

   # Define points on the opacity map curve
   # Each point is defined as [scalar data value, opacity level, midpoint, sharpness]
   # For a piecewise linear function, midpoint = 0.5 and sharpness = 0.0
   opacityMap.Points = [50, 1.0, 0.5, 0.0, 100, 0.7, 0.5, 0.0, 200, 0.0, 0.5, 0.0]


On retrouve l'activation de la map pour l'opacité qui sera associé au champ de valeurs
qui a servi pour réaliser l'extraction de contours. Cette map est décrite par
une liste à plat qui décrit une liste de tuple de 4 valeurs.

Dans cet exemple, il y en a 3 :

.. code-block:: python

   [  50, 1.0, 0.5, 0.0,
     100, 0.7, 0.5, 0.0,
     200, 0.0, 0.5, 0.0]

La signification de ces 4 valeurs présentées au-dessus sous la forme de colonne est la suivante :

* la première décrit la valeur du champ scalaire auquel on associe la description
  de l'opacité qui va suivre (ici, les valeurs retenues sont 50, 100 ou 200 ;
  il n'est pas nécessaire de décrire toutes les valeurs utilisées par l'iso-contour),

* la deuxième décrit le niveau d'opacité qui est une valeur comprise entre 0.0 (totalement
  transparent) et 1.0 (non transparent dont complétement opaque) ;
  généralement, on optera à donner une opacité plus élevé pour une petite valeur du champ
  que pour une grande,

* la troisième décrit le point médian, usuellement 0.5,

* la quatrième décrit la netteté, usuellement à 0.0.

Pour les deux dernières valeurs, ParaView n'en dit pas plus sur leur signification et usage ;
tous les exemples que nous avons pu croisé proposent ces valeurs usuelles.


.. |image0| image:: ../../img/procedures/04ContourFilterSelection.png
.. |image1| image:: ../../img/procedures/04ContourFilterAppearance.png
.. |image2| image:: ../../img/procedures/04ContourFilterOptions.png
.. |image3| image:: ../../img/procedures/04ContourFilterValueGenerator.png
.. |image4| image:: ../../img/procedures/04ContourFilterOpacityCommon.png
.. |image5| image:: ../../img/procedures/04EditColorMap.png
.. |image6| image:: ../../img/procedures/04ContourFilterOpacityLinear.png
.. |image7| image:: ../../img/procedures/04ContourFilterOpacityCustom.png
