Documentation: `doc en <../../en/procedures/ExportSelection.html>`__



Export a selection generated from picking
=========================================

Description
-----------

Once a selection operation is done, we typically want to extract it or
analyze it. We’ll see here how to make a selection using picking and
then export it to a file.

Procedure using the GUI
-----------------------

1: Do the picking
~~~~~~~~~~~~~~~~~

The toolbar located above the visualization window contains various
tools to select cells and points from a data set. Some of these tools
will be detailed here, starting with the ``Select Cells/Points On``
buttons. After clicking on one of these buttons, simply click in the
visualization window and drag to define a selection rectangle.

|image0|

Upon releasing, points or cells inside of the rectangle are highlighted,
indicating that they have been selected.

|image1|

Note that only points and cells closest to the camera (in terms of
depth) are selected. Elements hidden by others are not selected. To
select all points or cells that fit inside of the rectangle including
hidden ones, use the ``Select Cells/Points Through`` buttons. As
previously, click and drag to define a selection rectangle, then
release.

|image2|

Elements are now selected through the data set as shown below for cells.

|image3|

Instead of a rectangle, it is possible to draw a shape in a freehanded
manner by using the ``Select Cells/Points With Polygon`` buttons as
illustrated below.

|image4|

|image5|

It is also possible to do selection cell by cell or point by point
interactively. For that click on the ``Interactive Select Cells On`` or
the ``Interactive Select Points On`` buttons, then click on the cells or
point you want to select. A message will popup to introduce you on how
to use these mode. Keep in mind that while in this mode you cannot move
the camera using your mouse.

|image6|

By default each of these operations will create a new selection. If you
want to add your new selection to your current selection then you can
toggle the ``Add Selection`` mode with the corresponding button so each
subsequent selection you make is added to the previous one. Instead of
using the button you can also hold the ``Ctrl`` button pressed while
doing your selection. Similarly there is a ``Substract Selection`` mode
(shortcut ``Shift``) and a ``Toggle Selection`` mode (shortcut
``Ctrl+Shift``). The interactive selection explained just above is an
exception and will always add the newly selected cell or point to the
current selection by default, but still works with the ``Substract`` and
``Toggle`` mode.

|image7|

It can be useful to add information on the neighbor cells or points to
an existing selection. To achieve this, click on the dedicated
``Grow Selection`` button shown below. Use the ``Shrink Selection``
button to undo this operation.

|image8|

Lastly, you can clear a selection by clicking on the corresponding
button.

|image9|

2: Export your selection
~~~~~~~~~~~~~~~~~~~~~~~~

Once the selection is done it is first necessary to extract it before
exporting it. For that create an ``Extract Selection`` filter and click
on ``Apply``. If you change your selection after the creation of the
filter, the extracted selection will not update. This is because this
filter makes a copy of the current selection upon its creation. If you
want to extract a new selection using the same filter use the
``Copy Active Selection`` button. See below for an example where a
selection has been previously extracted but needs to be changed.

|image10|

Once your selection is extracted, make sure the ``Extract Selection``
filter is selected and go to ``File - Save Data`` to export it. In the
first dialog you’ll be able to choose where to save it and which format
to use. Once you validated this step a second dialog will open with
exporting parameters specific to the file format you choosed.

Procedure using Python scripting
--------------------------------

Irrelevant for picking.

.. |image0| image:: ../../img/procedures/06SelectionOn.png
.. |image1| image:: ../../img/procedures/06SelectionDone.png
.. |image2| image:: ../../img/procedures/06SelectionThrough.png
.. |image3| image:: ../../img/procedures/06SelectionThroughDone.png
.. |image4| image:: ../../img/procedures/06SelectionPolygon.png
.. |image5| image:: ../../img/procedures/06SelectionPolygonDone.png
.. |image6| image:: ../../img/procedures/06InteractiveSelection.png
.. |image7| image:: ../../img/procedures/06SelectionModeButtons.png
.. |image8| image:: ../../img/procedures/08GrowSelection.png
.. |image9| image:: ../../img/procedures/06SelectionClear.png
.. |image10| image:: ../../img/procedures/06ExtractSelectionFilter.png
