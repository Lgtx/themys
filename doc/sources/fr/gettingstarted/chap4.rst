Documentation: `doc en <../../en/gettingstarted/chap4.html>`__



Application de filtres
======================

Selon le paradigme du flux de données, on crée des pipelines avec des filtres pour transformer les données.
Semblable au menu **Sources**, qui nous permet de créer de nouvelles sources de données, il existe un menu
**Filters** qui permet d’accéder au grand ensemble de filtres disponibles dans **Themys**. Si vous parcourez
les éléments de ce menu, certains d’entre eux seront activés et d’autres seront désactivés. Les filtres qui
peuvent fonctionner avec le type de données produit par la source de sphère sont activés, tandis que d’autres
sont désactivés. Vous pouvez cliquer sur l’un des filtres activés pour créer une nouvelle instance de ce
type de filtre.

.. note::
   Pour comprendre pourquoi un filtre particulier ne fonctionne pas avec la source actuelle, déplacez simplement
   votre souris sur l’élément désactivé dans le menu Filtres. La barre d’état fournit une brève explication de la
   raison pour laquelle ce filtre n’est pas disponible.

Par exemple, si vous cliquez sur **Filters**>**Shrink**, cela créera un filtre qui réduira chacune des
cellules de maillage d’un facteur fixe. Exactement comme précédemment, lorsque nous avons créé la source
de sphère, nous voyons que le filtre nouvellement créé reçoit un nouveau nom, *Shrink1*, et est mis en
surbrillance dans le **Pipeline Browser**. Le panneau **Properties** est également mis à jour pour afficher
les propriétés de ce nouveau filtre, et le bouton **Apply** est mis en surbrillance pour nous demander
d’accepter les propriétés du filtre afin qu’il puisse être exécuté et que le résultat puisse être rendu.
Si vous cliquez sur *Sphere1* et *Shrink1* dans le **Pipeline Browser**, le panneau **Properties** et les
barres d’outils se mettent à jour, reflétant l’état du module de pipeline sélectionné.  C’est un concept
important dans Themys. Il existe une notion de module de pipeline actif, appelé source active. Plusieurs
panneaux, barres d’outils et menus seront mis à jour en fonction de la source active.

.. note::
   Pour rechercher un filtre, vous pouvez taper **CTRL^espace** puis commencer à taper les premières
   lettres de ce filtre.

Si vous cliquez sur **Apply**, comme c’était le cas auparavant, le filtre **Shrink** (rétrécir) sera exécuté
et le jeu de données résultant sera généré et affiché dans la vue 3D. **Themys** masquera également
automatiquement le résultat de la *Sphere1* afin qu’il ne soit pas affiché dans la vue. Sinon, les deux jeux
de données se chevaucheront. Cela se reflète dans le changement d’état des icônes de globe oculaire dans
l’explorateur de pipelines à côté de chacun des modules de pipeline. Vous pouvez afficher ou masquer les
résultats de n’importe quel module de pipeline en cliquant sur les globes oculaires.

Ce flux de travail simple constitue la base de toute l’analyse et de la visualisation des données dans
**Themys**. Le processus implique la création de sources et de filtres, la modification de leurs paramètres
et l’affichage du résultat généré dans une ou plusieurs vues. Dans la suite de ce guide, nous couvrirons
différents types de filtres et de traitement de données que vous pouvez effectuer. Nous couvrirons également
différents types de vues qui peuvent vous aider à produire un large éventail de visualisations 2D et 3D,
ainsi qu’à inspecter vos données et à les analyser.

.. warning::
   L'**erreur commune du débutant** est d'oublier souvent d’appuyer sur le bouton **Apply** après avoir
   créé un source / filtre ou après en avoir modifié les propriétés. C’est l’un des pièges les plus
   courants pour les utilisateurs novices dans le flux de travail **Themys**.
