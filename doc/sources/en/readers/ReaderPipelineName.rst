Documentation: `doc fr <../../fr/readers/ReaderPipelineName.html>`__



Configuring pipeline name for reader
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, **Themys** uses the name of the file as the name of the player instance
to create in the Pipeline Browser (**Pipeline Browser**). This is done as soon as
the object is created in the pipeline and will no longer be modifiable by this instance.

Very often, this choice is not the best, that's why it is possible in **Themys**
to modify the name of any element of the pipeline by double-clicking on it or by
using the context menu. It is thus possible to change the name of the instance of
a reader.

A recent evolution in **Themys** allows when creating the player instance to use
a name that is returned by the player itself. This is done through a property of
type *String* named **RegistrationName** to which are associated the eponymous
services of prefixed access *Get* and prefixed positioning *Set*.
This property is automatically called after the player instance is created to be
used to name the newly created object in the pipeline explorer. It is up to the
reader to suggest a better naming.

If this property does not exist, we return to the initial operating mode which
uses the name of the file.

An implementation corresponding to the default operation would give this:

.. code:: xml

   <StringVectorProperty
      label="Registration Name"
      name="RegistrationName"
      number_of_elements="1"
      command="GetFileName"
      panel_visibility="never"
      information_only="1">
   </StringVectorProperty>

The important thing is the name of this property **RegistrationName**, the
name of the method can be modified by modifying the attribute *command*.

An example of definition and use of this property is made in the Hercule reader.

.. note::
   Remember, the **RegistrationName** property is necessarily a *String*. The command must
   return a pointer to a *String* whose existence must persist when called at the reader level.
