Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_MaterialArray.html>`__


Material Array
^^^^^^^^^^^^^^

The **Material Array** property lists the different materials present in the different simulation meshes.
The user can thus select the simulation materials that seem of interest for the analysis to be carried out.

The materials prefixed by **global_** correspond to the global representation of each of the meshes of
simulation of the base. Indeed, certain value fields are only available at the global mesh level.

By default, they are not selected because they consume a lot of memory and are unsuitable for the loading of targeted materials.

However, in some cases, there is no other choice than to select the global version of a simulation mesh.
This is generally the case for clouds of particles or rays.

The name of the materials are prefixed with a number that is the value of the **vtkMaterialId** and **vtkFieldMaterialId** field.

Usually this selection should be done after the `mesh <HerculeReaders_Properties_MeshArray.html>`_ selection and before the 'cells and nodes fields <HerculeReaders_Properties_DataArray.html>`_ selection.
Of course, those choices should be coherent. It's only after all these selections done that the user may confirm his choice and trigger the loading
by clicking on **Apply**.

At any time the user may change the selections made.

.. warning::
   It is strongly advised to load only the minimum, because it is what will be loaded when the timestep changes
   or during the application of a filter that works on all timesteps (**Plot Data Over Time** for example).
