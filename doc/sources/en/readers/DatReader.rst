Documentation: `doc fr <../../fr/readers/DatReader.html>`__



Reader DAT
==========

Description
-----------

Le filtre ``CEAReaderDat`` permet de lire un fichier de type ``.dat``.

Le format de fichier .dat
=========================

Le format de fichier ``.dat`` est la description d’une liste de
poly-lignes dans un fichier ``ASCII``. Une poly-ligne est décrit par une
liste de points de l’espace reliés entre eux.

Voici un exemple de fichier ``.dat`` décrivant deux poly-lignes.

::

   -1.0000000000 0.0000000000
   0.0000000000 1.0000000000
   1.0000000000 0.0000000000
   0.0000000000 -1.0000000000
   &
   -2.0000000000 0.0000000000
   0.0000000000 2.0000000000
   2.0000000000 0.0000000000
   &

Chaque ligne décrit : - soit un point de l’espace par un tuple de deux
ou trois valeurs flottantes ; - soit un séparateur ``&`` entre deux
poly-lignes. Le fichier se termine par ce genre de ligne.

Option
------

Z Epsilon
~~~~~~~~~

Le ``Z Epsilon`` permet de fixer une translation dans la direction
``Z``. Par défaut, la valeur est à 0.01.

Dans le cas de multi poly-lignes dans l’espace 3D, la valeur du
``Z Epsilon`` doit être placée à 0.

Développement possible
----------------------

-  positionner automatiquement la valeur de ``Z Epsilon`` à 0 si un des
   points du fichier ``.dat`` est décrit dans l’espace 3D avec une
   valeur de ``Z`` non nulle
