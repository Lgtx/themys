Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_FixedTime.html>`__


Fixed Time
^^^^^^^^^^

This property allows to handle the choice of the simulation timestep to load.

This choice is made through a selection in a scrolling menu:

* starting with a blank line;
* then holding a list of all timesteps that are present in the database.

Selecting a simulation timestep will **immutably** fix the loading time of the database.
Events linked to a choice at the **Themys GUI** level will not have any effect anymore.
The `Time Shift <HerculeReaders_Properties_TimeShift.html>`_ property will be ignored.

Selecting the blank line will deactivate this property. The **Themys GUI** finds back its initial role.
