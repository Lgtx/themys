Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_Version.html>`__


Version
^^^^^^^

The **Version** property is available only with advanced display (using the gear button). It displays the version number of the Hercule reader.
