Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_TB-AMR.html>`__


HTG up to level max
^^^^^^^^^^^^^^^^^^^

This property set the maximal depth that should be loaded and kept in memory.

A null value keeps only the first depth level.

In the same spirit, it is also possible to apply a **vtkHyperTreeGridDepthLimiter** filter.
This will not reduce the memory cost (unlike this reader option) but it may speed up
the application of subsequent filters by limiting the levels taken into account.
