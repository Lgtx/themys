Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_MemoryEfficient.html>`__


Memory efficient
^^^^^^^^^^^^^^^^

This property allows to spare memory, by a factor of 2, when loading a floating point value field.
The type of this floating point field will then be a *float* instead of a *double*.
