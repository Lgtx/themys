Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_Extruder1D.html>`__


Extrude a 1D mesh
^^^^^^^^^^^^^^^^^

The loading of a 1D **vtkStructuredGrid** mesh is automatically transformed in a **vtkStructuredGrid** 2D
under the form of a *peacock tail*.

The properties of this transformation are:

* ``1D sector nb``: sets the number of cells;
* ``1D sector min``: sets the position of the first sector;
* ``1D sector max``: sets the position of the last sector.
