Documentation: `doc fr <../../fr/readers/readers.html>`__

Readers documentation
=====================

.. toctree::
   :maxdepth: 1

   DatReader
   HerculeReaders

Developer oriented section:

.. toctree::
   :maxdepth: 1

   ReaderPipelineName
