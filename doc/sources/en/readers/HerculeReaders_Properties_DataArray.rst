Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_DataArray.html>`__


Cell and Point Data Array
^^^^^^^^^^^^^^^^^^^^^^^^^

**Cell Data Array** and **Point Data Array** properties list the different value fields available
on the different materials and meshes present in the database.

The user may select the value fields that he wants to visualize.

With the **Hercule Services Reader** reader, the value fields prefixed by:

* **Maillage** are linked to the materials prefixed by **global_** ;
* **Milieu** are linked to unprefixed materials.

Thus, selecting **Material:Density** loads all density fields that are present in the selected materials.

With the **Hercule HIc Reader** reader, the value fields suffixed by **(global)** are linked to materials prefixed by **global_**.
Unprefixed value fields are themselves linked to unprefixed materials.

This becomes somewhat complicated when we introduce the **Constituent** and **Fluid** aspects whose fields names
 will necessarily be prefixed by the name of the constituent or fluid which is one of the components of a material.
The reason is to differentiate them from each other. If each of the materials has different constituents and fluids
this further multiplies the list of proposed value fields.

Usually this selection should be done after the `meshes <HerculeReaders_Properties_MeshArray.html>`_ and the `materials <HerculeReaders_Properties_MaterialArray.html>`_ selections.
Of course, those choices should be coherent. It's only after all these selections done that the user may confirm his choice and triggers the loading
by clicking on **Apply**.

At any time the user may change the selections made.

.. warning::
   It is strongly advised to load only the minimum, because it is what will be loaded when the timestep changes
   or during the application of a filter that works on all timesteps (**Plot Data Over Time** for example).
