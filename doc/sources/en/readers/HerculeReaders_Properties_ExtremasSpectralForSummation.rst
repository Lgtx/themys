Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_ExtremasSpectralForSummation.html>`__


Extremas spectral for summation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The loading of a spectral value fields is costly because each cell may hold hundreds of values.

That's why each spectral value field is proposed under three forms:

* its **whole vectorial version**, for example: *Milieu:Spectrum*;
* its **partial vectorial version**, for example: *Milieu:vtkPart_Spectrum*;
* its **summed partial scalar version**, for example: *Milieu:vtkSum_Spectrum*.

The first item is the whole value fields as it is stored in the Hercule database.

The two last items allow to operate a reduction operation when the spectral field is loaded.

The values of **Extremas spectral for summation** property determine the indices that will be used to realise the
reduction operation:

* for the extraction of a part of the spectral field, *vtkPart_*;
* or to realize the partial summation, *vtkSum_*.

The first value of this property is the first index that will be used in the reduction operation.
The -1 value is equivalent to 0.

The second value is the last index that will be used in the reduction operation.
If it is equal to -1, the last item of the spectral field will be taken into account.
