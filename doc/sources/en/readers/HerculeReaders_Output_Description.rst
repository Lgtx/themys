Documentation: `doc fr <../../fr/readers/HerculeReaders_Output_Description.html>`__

Description of the ``vtk`` structure obtained after reading
-----------------------------------------------------------

Hercule readers **Hercule Services Reader** and **Hercule HIc Reader** allow to read data stored with the **Hercule**
format that is used at CEA/DAM.

For both readers, the **output** structure is the following:

* the root is a **vtkMultiBlockDataSet** describing a list of **blocks** / **meshes** that represent
  the meshes, particles or laser present in the database;

* each **block** / **mesh** is itself a **vtkMultiBlockDataSet** describing a list of **blocks** / **materials** / **mediums** / **groups**
  representing part of the simulation mesh. Given the simulation type, a geometrical overlapping between this mesh parts may exist.
  It could be partial or total. Cells belonging to more than one of those overlapping parts are called **mixed cells*.

* each **block** / **material** / **medium** / **group** is a **vtkMultiPieceDataSet** representing:

  * the distribution of the visualization servers when using the **Hercule Services Reader**;

  * a mix between the distribution of the visualization servers and the distribution of computation subdomains
    when using the **Hercule HIc Reader**

* all of the **pieces** of a **vtkMultiPieceDataSet** are necessarily:

  * structured: **vtkImageData** (2-3D), **vtkRectilinearGrid** (2-3D), **vtkStructuredGrid** (1-2-3D);

  * unstructured: **vtkPolyData** (1-2-3D), **vtkUnstructuredGrid** (1-2-3D);

  * and, when using the **Hercule HIc Reader** for TB-AMR(HTG) databases: **vtkHyperTreeGrid** (2-3D).

If a 1D database is loaded, then the Hercule readers will produce a 2D **vtkStructuredGrid** thanks to a rotational extrusion.
The parameters of such extrusion are set in the readers properties.

Value fields are associated to the cells (**CellData**) or to the nodes (**PointData**) of each **piece**,
except in the case of **vtkHyperTreeGrid** where they are associated only to the cells.

Global value fields (**FieldData**) are also associated to each of the **pieces**.

They are unique:

* to the **piece**, as:

  * **vtkFieldMaterialId**: **material** / **medium** / **group** id;

  * **vtkProcessId**: **Themys** running server id, if **Themys** has been launched in client/servers mode;

* to the simulation, the global field is then duplicated on each **piece**:

  * **vtkFixedTimeStep** : index of the timestep current loaded;

  * **vtkFixedTimeValue** : value of the timestep current loaded;

  * **Place** and **DistanceToOrigin** : symmetry plane and its type (0 is X, 1 is Y and 2 is Z) and the
    distance to the origin of the plane.
