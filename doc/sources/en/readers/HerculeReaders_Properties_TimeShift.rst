Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_TimeShift.html>`__


Time Shift
^^^^^^^^^^

This property applies a shift (positive or negative) to the timestep selected in the **Themys GUI**.
It can be useful when comparing two databases that do not share a common time origin.

The value of this property is simply added to the timestep selected in the **Themys GUI**.
This value is then used by the reader to load the corresponding time in the database
(see `Current Time <HerculeReaders_Properties_CurrentTime.html>`_ for more informations).

Setting this property's value to zero deactivates it.

This property is ignored if a timestep has been set through the `Fixed Time <HerculeReaders_Properties_FixedTime.html>`_ property.
