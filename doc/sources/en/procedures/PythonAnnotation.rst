Documentation: `doc fr <../../fr/procedures/PythonAnnotation.html>`__



Create an annotation
====================

Description
-----------

Annotations are used to display on-screen textual information, such as
time, data values, or comments. The annotation filter described here is
the Python annotation, which is highly versatile.

Procedure using the GUI
-----------------------

Start by activating the ``Advanced Mode`` by clicking on the
corresponding button shown below. After selecting the data set from the
``Pipeline Browser`` panel by clicking on it, go into
``Filters > Annotation > Python Annotation`` in the menu.

|image0|

In the ``Properties`` panel, the ``Array Association`` combo box
specifies which type of data array can be referenced in the annotation.
Then, enter the annotation in the ``Expression`` input box. The example
below displays the following expression:
``"Current time step is %f $\mu$s" % (time_value * 1000000)``.

|image1|

Note that ``time_value`` above is a variable defined for temporal data
sets. Basic LaTeX formatting is also possible by enclosing text inside
``$`` symbols.

Other examples are available `here <#annotation-examples>`__. For more
details on the various annotation possibilities, see `this
page <https://docs.paraview.org/en/latest/ReferenceManual/annotations.html?#python-annotation-filter>`__.

Options to move the annotation inside of the visualization window and to
change the text properties are given in the ``Display`` section of the
``Properties`` panel (boxed in red and blue below, respectively).

For the location of the annotation, you can either choose a fixed
position inside of the window, or choose ``Use Coordinates`` to directly
click on the annotation and drag it to the desired position.

|image2|

Annotation examples
-------------------

This section contains several examples of annotations including
variables and special formatting. For longer annotations, you may want
to write the text string in a text editor before pasting it in the
``Expression`` input box.

-  Text and variable:
   ``"Current time step is %f ms" % (time_value * 1000)`` (or t_value
   variable)

|image3|

-  Cell data array value for one element (2) of one block in a
   multiblock (0):
   ``"ScalarData value for cell 3: %f" % ScalarData[2].Arrays[0]``

|image4|

-  Multiple rows and columns: ``"A | B\nC | D"``

|image5|

-  Other expression:

   -  ``"Date %s\n" % (date.today())``
   -  ``"Summation RTData: %f\n" % (numpy.cumsum(inputs[0].PointData["RTData"])[-1])``
   -  ``"Summation RTData: %f\n" % (numpy.sum(inputs[0].PointData["RTData"]))``
   -  ``"Avg Density: %f\n" % (numpy.average(inputs[0].CellData["Milieu:Density"]))``

You can combine these different syntaxes.

To define a new text row or column, use the character ``\n`` or ``|``,
respectively. In this case several options are available to modify the
spacing between cells as well as draw interior borders.

|image6|

Procedure using Python scripting
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'can.ex2' according to the data set to annotate
   mySource = FindSource('can.ex2')

   # Create a new 'PythonAnnotation' filter
   annotation = PythonAnnotation(Input=mySource)

   # Define annotation
   annotation.Expression = '"Current time step is %f ms" % (time_value * 1000)'

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   annotationDisplay = Show(annotation, renderView)

   # Change window location of annotation
   annotationDisplay.WindowLocation = 'Lower Center'

   # Manually set coordinates for the lower left corner of the annotation box
   # Position is changed only if the window location is 'Any Location'
   annotationDisplay.WindowLocation = 'Any Location'
   annotationDisplay.Position = [0.6, 0.05]

.. |image0| image:: ../../img/procedures/11PythonAnnotation.png
.. |image1| image:: ../../img/procedures/11ExampleAnnotation.png
.. |image2| image:: ../../img/procedures/11AnnotationParameters.png
.. |image3| image:: ../../img/procedures/11ExampleAnnotation1.png
.. |image4| image:: ../../img/procedures/11ExampleAnnotation2.png
.. |image5| image:: ../../img/procedures/11ExampleAnnotation3.png
.. |image6| image:: ../../img/procedures/11MultiCellOptions.png
