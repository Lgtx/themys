Documentation: `doc fr <../../fr/procedures/axes.html>`__



Axes
====

Paraview propose différents modes de rendu des axes.

Dans les propriétés (**Properties**), section affichage (**Display**), il est possible d'afficher les axes sur
cadre de délimitation globale de l'objet sélectionné dans le navigateur du pipeline (**Pipeline Browser**).


**TODO**

- les différents modes de rendus des axes
- la modification des propriétés

Dans le mode avancé, il est proposé de rendre les axes en mode polaire avec ou sans excentricité (``ratio``).
