Documentation: `doc fr <../../fr/procedures/SelectionWithGeometry.html>`__



Define a selection using geometrical objects
============================================

Description
-----------

The selection operation highlights a subset of elements in a data set,
typically cells or points, which can then be extracted or analyzed.
Selection using geometrical objects in particular uses shapes such as
polygons, spheres, etc. to define a selection.

Procedure using mouse selection tools
-------------------------------------

The toolbar located above the visualization window contains various
tools to select cells and points from a data set. Some of these tools
will be detailed here, starting with the ``Select Cells/Points On``
buttons. After clicking on one of these buttons, simply click in the
visualization window and drag to define a selection rectangle.

|image0|

Upon releasing, points or cells inside of the rectangle are highlighted,
indicating that they have been selected.

|image1|

Note that only points and cells closest to the camera (in terms of
depth) are selected. Elements hidden by others are not selected. To
select all points or cells that fit inside of the rectangle including
hidden ones, use the ``Select Cells/Points Through`` buttons. As
previously, click and drag to define a selection rectangle, then
release.

|image2|

Elements are now selected through the data set as shown below for cells.

|image3|

Instead of a rectangle, it is possible to draw a shape in a freehanded
manner by using the ``Select Cells/Points With Polygon`` buttons as
illustrated below.

|image4|

|image5|

Lastly, you can clear a selection by clicking on the corresponding
button.

|image6|

Procedure using a sphere, box or cylinder
-----------------------------------------

Worth mentioning is the ``Clip`` filter which can be used to extract a
subset of a data set (rather than select) using a sphere, a box or a
cylinder.

First select the data set from the ``Pipeline Browser`` panel by
clicking on it. Then click on the ``Clip`` filter in the filter toolbar
as shown below.

|image7|

Select the geometrical object to use in the ``Clip Type`` combo box.
Tick the ``Crinkle Clip`` as well to disable cell clipping. Parameters
such as radius, position, etc. can be modified to adjust the extraction
shape. Lastly, click on ``Apply`` to execute the extraction.

|image8|

|image9|

|image10|

|image11|

.. |image0| image:: ../../img/procedures/06SelectionOn.png
.. |image1| image:: ../../img/procedures/06SelectionDone.png
.. |image2| image:: ../../img/procedures/06SelectionThrough.png
.. |image3| image:: ../../img/procedures/06SelectionThroughDone.png
.. |image4| image:: ../../img/procedures/06SelectionPolygon.png
.. |image5| image:: ../../img/procedures/06SelectionPolygonDone.png
.. |image6| image:: ../../img/procedures/06SelectionClear.png
.. |image7| image:: ../../img/procedures/02ClipFilterSelection.png
.. |image8| image:: ../../img/procedures/06SelectionClip.png
.. |image9| image:: ../../img/procedures/06SelectionClipSphere.png
.. |image10| image:: ../../img/procedures/06SelectionClipBox.png
.. |image11| image:: ../../img/procedures/06SelectionClipCylinder.png
