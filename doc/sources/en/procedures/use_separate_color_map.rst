Documentation: `doc fr <../../fr/procedures/use_separate_color_map.html>`__



Utilisation d'une palette différente entre bases
------------------------------------------------

Une fois un élément actif dans l'explorateur de pipeline, il est possible de lui attribuer
une palette différente en cliquant simplement sur le troisième bouton référençant l'aspect coloration
au niveau de la barre des menus :

# le premier permet de rendre visible la légende de couleur (**Toggle Color Legend Visibility**) ;

# le second permet d'ouvrir la vue d'édition de la palette des couleurs (**Edit Color map**) ;

# le troisième permet de dissocier l'utilisation de la palette pour la base en cours
  de sélection (**Use Separate Color Map**).
