Documentation: `doc fr <../../fr/procedures/field_vector.html>`__



Glyph / Champ de vecteurs
=========================

Description
===========

Sur une cellule ou un noeud, un champ vectoriel peut être représenté par une couleur
cartographie basée sur la magnitude vectorielle (racine carrée de la somme des
carré de chaque composant). Au lieu de cela, les glyphes peuvent être utilisés pour afficher le vecteur
champs de géométries différentes.

Procédure à l'aide de l'interface graphique
-------------------------------------------

Commencez par sélectionner le jeu de données contenant le champ vectoriel à afficher dans le panneau ``Pipeline Browser`` en cliquant dessus. Sélectionnez la représentation ``Glyphes 3D`` dans la liste déroulante correspondante, comme illustré ci-dessous.

|image0|

Les options pour modifier cette représentation sont présentes dans la section ``Display`` du panneau ``Properties``. Par défaut, seul ``Glyph Type`` est disponible, qui est défini sur ``Arrow`` (flèche).

|image1|

Pour accéder à plus d'options, cliquez sur le bouton de la roue pour ``Advanced Properties``.

Cochez d'abord l'option ``Orient`` afin que les flèches pointent dans la direction du champ vectoriel sélectionné dans la liste déroulante ``Orientation Vectors`` ci-dessous.

|image2|

Vous pouvez également mettre à l'échelle la longueur des flèches en cochant l'option ``Scaling``. Choisissez le champ vectoriel à mettre à l'échelle avec la liste déroulante ``Scale Array``. Le facteur d'échelle peut être ajusté pour s'adapter à vos données.

|image3|

Enfin, il est possible d'affiner l'apparence des flèches en modifiant la longueur de la pointe et de la tige, le rayon et la résolution.

|image4|

Plusieurs autres types de glyphes sont disponibles, chacun avec des options spécifiques pour modifier leur apparence, comme indiqué ci-dessous.

|image5|

|image6|

|image7|

|image8|

|image9|

|image10|

|image11|

|image12|

Procédure utilisant les scripts Python
--------------------------------------

1. Flèche (Arrow)
~~~~~~~~~~~~~~~~~

.. code:: py

   # Trouver l'ensemble de données source avec son nom
   # Remplacez 'Sphere1' en fonction de l'ensemble de données souhaité
   mySource = FindSource('Sphere1')

   # Obtenir les propriétés d'affichage de la source
   sphereDisplay = GetDisplayProperties(mySource)

   # Définir le type de représentation sur les glyphes 3D
   sphereDisplay.SetRepresentationType('3D Glyphs')

   # Si nécessaire, orientez les glyphes à l'aide d'un champ vectoriel
   sphereDisplay.Orient = 1

   # Définir le champ vectoriel pour l'orientation
   # Remplace 'Normals' avec un tableau vectoriel valide
   sphereDisplay.SelectOrientationVectors = 'Normals'

   # Si nécessaire, mettez à l'échelle les glyphes à l'aide d'un champ vectoriel
   sphereDisplay.Scaling = 1

   # Définir le champ vectoriel pour la mise à l'échelle
   # Remplace 'Normals' avec un tableau vectoriel valide
   sphereDisplay.SelectScaleArray = 'Normals'

   # Définir s'il faut mettre à l'échelle avec la magnitude vectorielle ou les composants
   sphereDisplay.ScaleMode = 'Magnitude'
   # ou
   sphereDisplay.ScaleMode = 'Vector Components'

   # Définir le facteur d'échelle
   sphereDisplay.ScaleFactor = 0.2

   # Change l'apprence du glyphe
   sphereDisplay.GlyphType.TipResolution = 15
   sphereDisplay.GlyphType.TipRadius = 0.2
   sphereDisplay.GlyphType.TipLength = 0.4
   sphereDisplay.GlyphType.ShaftResolution = 15
   sphereDisplay.GlyphType.ShaftRadius = 0.08

2. Cône (Cone)
~~~~~~~~~~~~~~

.. code:: py

   # Remplacez le type de glyphe par cone
   sphereDisplay.GlyphType = 'Cone'
   sphereDisplay.GlyphType.Resolution = 20
   sphereDisplay.GlyphType.Radius = 0.4
   sphereDisplay.GlyphType.Height = 1.5
   sphereDisplay.GlyphType.Center = [1.0, 0.0, 0.0]
   sphereDisplay.GlyphType.Direction = [1.0, 0.0, 0.0]

3. Boîte (Box)
~~~~~~~~~~~~~~

.. code:: py

   # Remplacez le type de glyphe par box
   sphereDisplay.GlyphType = 'Box'
   sphereDisplay.GlyphType.XLength = 1.0
   sphereDisplay.GlyphType.YLength = 0.5
   sphereDisplay.GlyphType.ZLength = 0.5
   sphereDisplay.GlyphType.Center = [1.0, 0.0, 0.0]

4. Cylindre (Cylinder)
~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Remplacez le type de glyphe par cylinder
   sphereDisplay.GlyphType = 'Cylinder'
   sphereDisplay.GlyphType.Resolution = 20
   sphereDisplay.GlyphType.Height = 1.5
   sphereDisplay.GlyphType.Radius = 0.3
   sphereDisplay.GlyphType.Center = [1.0, 0.0, 0.0]

5. Ligne (Line)
~~~~~~~~~~~~~~~

.. code:: py

   # Remplacez le type de glyphe par line
   sphereDisplay.GlyphType = 'Line'
   sphereDisplay.GlyphType.Point1 = [1.0, 0.0, 0.0]
   sphereDisplay.GlyphType.Point2 = [2.0, 0.0, 0.0]
   sphereDisplay.GlyphType.Resolution = 10

6. Sphère (Sphere)
~~~~~~~~~~~~~~~~~~

.. code:: py

   # Remplacez le type de glyphe par sphere
   sphereDisplay.GlyphType = 'Sphere'
   sphereDisplay.GlyphType.Center = [1.0, 0.0, 0.0]
   sphereDisplay.GlyphType.Radius = 0.2
   sphereDisplay.GlyphType.ThetaResolution = 15
   sphereDisplay.GlyphType.StartTheta = 0
   sphereDisplay.GlyphType.EndTheta = 180
   sphereDisplay.GlyphType.PhiResolution = 15
   sphereDisplay.GlyphType.StartPhi = 0
   sphereDisplay.GlyphType.EndPhi = 90

7. 2D Glyphes (Glyphs)
~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Remplacez le type de glyphe par 2D glyph
   # Remplacez 'Diamond' par l'un des types de glyphes suivants:
   # Vertex, Dash, Cross, ThickCross, Triangle, Square, Circle,
   # Diamond, Arrow, ThickArrow, HookedArrow, EdgeArrow
   sphereDisplay.GlyphType = '2D Glyph'
   sphereDisplay.GlyphType.GlyphType = 'Diamond'
   sphereDisplay.GlyphType.Filled = 1
   sphereDisplay.GlyphType.Center = [1.0, 0.0, 0.0]

.. |image0| image:: ../../img/procedures/12Representation.png
.. |image1| image:: ../../img/procedures/12GlyphDefault.png
.. |image2| image:: ../../img/procedures/12GlyphOrientation.png
.. |image3| image:: ../../img/procedures/12GlyphScaling.png
.. |image4| image:: ../../img/procedures/12GlyphSize.png
.. |image5| image:: ../../img/procedures/12GlyphCone.png
.. |image6| image:: ../../img/procedures/12GlyphBox.png
.. |image7| image:: ../../img/procedures/12GlyphCylinder.png
.. |image8| image:: ../../img/procedures/12GlyphLine.png
.. |image9| image:: ../../img/procedures/12GlyphSphere.png
.. |image10| image:: ../../img/procedures/12Glyph2D.png
.. |image11| image:: ../../img/procedures/12GlyphUnfilledDiamond.png
.. |image12| image:: ../../img/procedures/12GlyphFilledDiamond.png
