Documentation: `doc fr <../../fr/procedures/numpy_interface.html>`__

===============
Numpy interface
===============

Disclaimer
----------

Those pages are a copy/paste of a `series of blog posts by Berk Geveci <https://www.kitware.com//improved-vtk-numpy-integration>`__

Part 1
------

Recently, we introduced a new Python module called
numpy_interface to VTK. The main objective of this module is to
make it easier to interface VTK and numpy. This article is the
first in a series that introduces this module. Let’s start with
a teaser.

.. code:: python

    import vtk
    from vtk.numpy_interface import dataset_adapter as dsa
    from vtk.numpy_interface import algorithms as algs
    s = vtk.vtkSphereSource()
    e = vtk.vtkElevationFilter()
    e.SetInputConnection(s.GetOutputPort())
    e.Update()
    sphere = dsa.WrapDataObject(e.GetOutput())
    print sphere.PointData.keys()
    print sphere.PointData['Elevation']

This example prints out the following (assuming that you have a
relatively new checkout of VTK master from git).

.. code:: python

    ['Normals', 'Elevation']
    [ 0.5         0.          0.45048442  0.3117449   0.11126047  0.          0.
      0.          0.45048442  0.3117449   0.11126047  0.          0.          0.
      0.45048442  0.3117449   0.11126047  0.          0.          0.
      0.45048442  0.3117449   0.11126047  0.          0.          0.
      0.45048442  0.3117449   0.11126047  0.          0.          0.
      0.45048442  0.3117449   0.11126047  0.          0.          0.
      0.45048442  0.3117449   0.11126047  0.          0.          0.
      0.45048442  0.3117449   0.11126047  0.          0.          0.        ]

The last three lines are what is new. Note how we used a
different API to access the PointData and the Elevation array
on the last two lines. Also note that when we printed the
Elevation array, the output didn’t look like one from a
vtkDataArray. In fact:

.. code:: python

    elevation = sphere.PointData['Elevation']
    print type(elevation)
    import numpy
    print isinstance(elevation, numpy.ndarray)

prints the following.

.. code:: python

    <class 'vtk.numpy_interface.dataset_adapter.VTKArray'>
    True

So a VTK array is a numpy array? What kind of trickery is this
you say? What kind of magic makes the following possible?

.. code:: python

    sphere.PointData.append(elevation + 1, 'e plus one')
    print algs.max(elevation)
    print algs.max(sphere.PointData['e plus one'])
    print sphere.VTKObject

Output:

.. code:: python

    0.5
    1.5
    vtkPolyData (0x7fa20d011c60)
        ...
        Point Data:
        ...
        Number Of Arrays: 3
        Array 0 name = Normals
        Array 1 name = Elevation
        Array 2 name = e plus one

It is all in the numpy_interface module. It ties VTK datasets
and data arrays to numpy arrays and introduces a number of
algorithms that can work on these objects. There is quite a bit
to this module and I will introduce it piece by piece in
upcoming blogs.

Let’s wrap up this blog with one final teaser:

.. code:: python

    w = vtk.vtkRTAnalyticSource()
    t = vtk.vtkDataSetTriangleFilter()
    t.SetInputConnection(w.GetOutputPort())
    t.Update()
    ugrid = dsa.WrapDataObject(t.GetOutput())
    print algs.gradient(ugrid.PointData['RTData'])

Output:

.. code:: python

    [[ 25.46767712   8.78654003   7.28477383]
     [  6.02292252   8.99845123   7.49668884]
     [  5.23528767   9.80230141   8.3005352 ]
     ...,
     [ -6.43249083  -4.27642155  -8.30053616]
     [ -5.19838905  -3.47257614  -7.49668884]
     [ 13.42047501  -3.26066017  -7.28477287]]

Please note that this example is not very easily replicated by
using pure numpy. The gradient function returns the gradient of
an unstructured grid – a concept that does not exist in numpy.
However, the ease-of-use of numpy is there.

Part 2
------

This is my second blog about the recently introduced
numpy_interface module. In the `first
one <https://blog.kitware.com/improved-vtk-numpy-integration/>`__,
I gave a brief overview of the module and shared a few teasers.
In this one, I will go over the dataset_adapter module which is
part of numpy_interface. This module was designed to simplify
accessing VTK datasets and arrays from Python and to provide a
numpy-style interface.

The first step in using the dataset_adapter module is to
convert an existing VTK dataset object to a
dataset_adapter.VTKObjectWrapper. Let’s see how this is done by
examining the teaser from the last blog:

.. code:: python

    import vtk
    from vtk.numpy_interface import dataset_adapter as dsa

    s = vtk.vtkSphereSource()

    e = vtk.vtkElevationFilter()
    e.SetInputConnection(s.GetOutputPort())
    e.Update()

    sphere = dsa.WrapDataObject(e.GetOutput())

    print sphere
    print isinstance(sphere, dsa.VTKObjectWrapper)

will print:

.. code:: python

    <vtk.numpy_interface.dataset_adapter.PolyData object at 0x1101fbb50>
    True

What we did here is to create an instance of the
dataset_adapter.PolyData class, which refers to the output of
the vtkElevationFilter filter. We can access the underlying VTK
object using the VTKObject member:

.. code:: python

    >> print type(sphere.VTKObject)
    <type 'vtkobject'>

Note that the WrapDataObject() function will return an
appropriate wrapper class for all vtkDataSet subclasses,
vtkTable and all vtkCompositeData subclasses. Other
vtkDataObject subclasses are not currently supported.

VTKObjectWrapper forwards VTK methods to its VTKObject so the
VTK API can be accessed directy as follows:

.. code:: python

    >> print sphere.GetNumberOfCells()
    96L

However, VTKObjectWrappers cannot be directly passed to VTK
methods as an argument.

.. code:: python

    >> s = vtk.vtkShrinkPolyData()
    >> s.SetInputData(sphere)
    TypeError: SetInputData argument 1: method requires a VTK object
    >> s.SetInputData(sphere.VTKObject)

Dataset Attributes
++++++++++++++++++

So far, pretty boring, right? We have a wrapper for VTK data
objects that partially behaves like a VTK data object. This
gets a little bit more interesting when we start looking how
one can access the fields (arrays) contained within this
dataset.

.. code:: python

    >> sphere.PointData
    <vtk.numpy_interface.dataset_adapter.DataSetAttributes at 0x110f5b750>

    >> sphere.PointData.keys()
    ['Normals', 'Elevation']

    >> sphere.CellData.keys()
    []

    >> sphere.PointData['Elevation']
    VTKArray([ 0.5       ,  0.        ,  0.45048442,  0.3117449 ,  0.11126047,
               0.        ,  0.        ,  0.        ,  0.45048442,  0.3117449 ,
               0.11126047,  0.        ,  0.        ,  0.        ,  0.45048442,
               ...,
               0.11126047,  0.        ,  0.        ,  0.        ,  0.45048442,
               0.3117449 ,  0.11126047,  0.        ,  0.        ,  0.        ], dtype=float32)

    >> elevation = sphere.PointData['Elevation']

    >> elevation[:5]
    VTKArray([0.5, 0., 0.45048442, 0.3117449, 0.11126047], dtype=float32)

Note that this works with composite datasets as well:

.. code:: python

    >> mb = vtk.vtkMultiBlockDataSet()
    >> mb.SetNumberOfBlocks(2)
    >> mb.SetBlock(0, sphere.VTKObject)
    >> mb.SetBlock(1, sphere.VTKObject)
    >> mbw = dsa.WrapDataObject(mb)
    >> mbw.PointData
    <vtk.numpy_interface.dataset_adapter.CompositeDataSetAttributes instance at 0x11109f758>

    >> mbw.PointData.keys()
    ['Normals', 'Elevation']

    >> mbw.PointData['Elevation']
    <vtk.numpy_interface.dataset_adapter.VTKCompositeDataArray at 0x1110a32d0>

It is possible to access PointData, CellData, FieldData, Points
(subclasses of vtkPointSet only), Polygons (vtkPolyData only)
this way. We will continue to add accessors to more types of
arrays through this API.

This is it for now. In my next blog in this series, I will talk
about the array API and various algorithms the numpy_interface
module provides.

Part 3
------

So far, I have briefly
`introduced <https://blog.kitware.com/improved-vtk-numpy-integration/>`__
the numpy_interface module and `discussed the dataset
interface <https://blog.kitware.com/improved-vtk-numpy-integration-part-2/>`__.
Finally, we get to something more interesting: working with
arrays, datasets and algorithms. This is where the
numpy_interface shines and makes certain data analysis tasks
significantly easier. Let’s start with a simple example.

.. code:: python

    from vtk.numpy_interface import dataset_adapter as dsa
    from vtk.numpy_interface import algorithms as algs
    import vtk

    w = vtk.vtkRTAnalyticSource()
    w.Update()
    image = dsa.WrapDataObject(w.GetOutput())
    rtdata = image.PointData['RTData']

    tets = vtk.vtkDataSetTriangleFilter()
    tets.SetInputConnection(w.GetOutputPort())
    tets.Update()
    ugrid = dsa.WrapDataObject(tets.GetOutput())
    rtdata2 = ugrid.PointData['RTData']

Here we created two datasets: an image data (vtkImageData) and
an unstructured grid (vtkUnstructuredGrid). They essentially
represent the same data but the unstructured grid is created by
tetrahedralizing the image data. So we expect that unstructured
grid to have the same points but more cells (tetrahedra).

Array API
+++++++++

Numpy_interface array objects behave very similar to numpy
arrays. In fact, arrays from vtkDataSet subclasses are
instances of VTKArray, which is a subclass of numpy.ndarray.
Arrays from vtkCompositeDataSet and subclasses are not numpy
arrays but behave very similarly. I will outline the
differences in a separate article. Let’s start with the basics.
All of the following work as expected.

.. code:: python

    >>> rtdata[0]
    60.763466

    >>> rtdata[-1]
    57.113735

    >>> rtdata[0:10:3]
    VTKArray([  60.76346588,   95.53707886,   94.97672272,  108.49817657], dtype=float32)

    >>> rtdata + 1
    VTKArray([ 61.
    76346588,  86.87795258,  73.80931091, ...,  68.51051331,
            44.34006882,  58.1137352 ], dtype=float32)

    >>> rtdata < 70
    VTKArray([ True , False, False, ...,  True,  True,  True], dtype=bool)

    # We will cover algorithms later. This is to generate a vector field.
    >>> avector = algs.gradient(rtdata)

    # To demonstrate that avector is really a vector
    >>> algs.shape(rtdata)
    (9261,)

    >>> algs.shape(avector)
    (9261, 3)

    >>> avector[:, 0]
    VTKArray([ 25.69367027,   6.59600449,   5.38400745, ...,  -6.58120966,
               -5.77147198,  13.19447994])

A few things to note in this example:

-  Single component arrays always have the following shape:
    (ntuples,) and not (ntuples, 1)
-  Multiple component arrays have the following shape:
    (ntuples, ncomponents)
-  Tensor arrays have the following shape: (ntuples, 3, 3)
-  The above holds even for images and other structured data.
    All arrays have 1 dimension (1 component arrays), 2
    dimensions (multi-component arrays) or 3 dimensions (tensor
    arrays).

One more cool thing. It is possible to use boolean arrays to
index arrays. So the following works very nicely:

.. code:: python

    >>> rtdata[rtdata < 70]
    VTKArray([ 60.76346588,  66.75043488,  69.19681549,  50.62128448,
               64.8801651 ,  57.72655106,  49.75050354,  65.05570221,
               57.38450241,  69.51113129,  64.24596405,  67.54656982,
               ...,
               61.18143463,  66.61872864,  55.39360428,  67.51051331,
               43.34006882,  57.1137352 ], dtype=float32)

    >>> avector[avector[:,0] > 10]
    VTKArray([[ 25.69367027,   9.01253319,   7.51076698],
              [ 13.1944809 ,   9.01253128,   7.51076508],
              [ 25.98717642,  -4.49800825,   7.80427408],
              ...,
              [ 12.9009738 , -16.86548471,  -7.80427504],
              [ 25.69366837,  -3.48665428,  -7.51076889],
              [ 13.19447994,  -3.48665524,  -7.51076794]])

Algorithms
++++++++++

One can do a lot simply using the array API. However, things
get much more interesting when we start using the
numpy_interface.algorithms module. I introduced it briefly in
the previous examples. I will expand on it a bit more here. For
a full list of algorithms, use help(algs). Here are some
self-explanatory examples:

.. code:: python

    >>> algs.sin(rtdata)
    VTKArray([-0.87873501, -0.86987603, -0.52497   , ..., -0.99943125,
              -0.59898132,  0.53547275], dtype=float32)

    >>> algs.min(rtdata)
    VTKArray(37.35310363769531)

    >>> algs.max(avector)
    VTKArray(34.781060218811035)

    >>> algs.max(avector, axis=0)
    VTKArray([ 34.78106022,  29.01940918,  18.34743023])

    >>> algs.max(avector, axis=1)
    VTKArray([ 25.69367027,   9.30603981,   9.88350773, ...,  -4.35762835,
               -3.78016186,  13.19447994])

If you haven’t used the axis argument before, it is pretty
easy. When you don’t pass an axis value, the function is
applied to all values of an array without any consideration for
dimensionality. When axis=0, the function will be applied to
each component of the array independently. When axis=1, the
function will be applied to each tuple independently.
Experiment if this is not clear to you. Functions that work
this way include sum, min, max, std and var.

Another interesting and useful function is where which returns
the indices of an array where a particular condition occurs.

.. code:: python

    >>> algs.where(rtdata < 40)
    (array([ 420, 9240]),)

For vectors, this will also return the component index if an
axis is not defined.

.. code:: python

    >>> algs.where(avector < -29.7)
    (VTKArray([4357, 4797, 4798, 4799, 5239]), VTKArray([1, 1, 1, 1, 1]))

So far, all of the functions that we discussed are directly
provided by numpy. Many of the numpy ufuncs are included in the
algorithms module. They all work with single arrays and
composite data arrays (more on this on another blog).
Algorithms also provides some functions that behave somewhat
differently than their numpy counterparts. These include cross,
dot, inverse, determinant, eigenvalue, eigenvector etc. All of
these functions are applied to each tuple rather than to a
whole array/matrix. For example:

.. code:: python

    >>> amatrix = algs.gradient(avector)
    >>> algs.determinant(amatrix)
    VTKArray([-1221.2732624 ,  -648.48272183,    -3.55133937, ...,    28.2577152 ,
               -629.28507693, -1205.81370163])

Note that everything above only leveraged per-tuple information
and did not rely on the mesh. One of VTK’s biggest strengths is
that its data model supports a large variety of meshes and its
algorithms work generically on all of these mesh types. The
algorithms module exposes some of this functionality. Other
functions can be easily implemented by leveraging existing VTK
filters. I used gradient before to generate a vector and a
matrix. Here it is again

.. code:: python

    >>> avector = algs.gradient(rtdata)
    >>> amatrix = algs.gradient(avector)

Functions like this require access to the dataset containing
the array and the associated mesh. This is one of the reasons
why we use a subclass of ndarray in dataset_adapter:

.. code:: python

    >>> rtdata.DataSet
    <vtk.numpy_interface.dataset_adapter.DataSet at 0x11b61e9d0>

Each array points to the dataset containing it. Functions such
as gradient use the mesh and the array together.Numpy provides
a gradient function too, you say. What is so exciting about
yours? Well, this:

.. code:: python

    >>> algs.gradient(rtdata2)
    VTKArray([[ 25.46767712,   8.78654003,   7.28477383],
              [  6.02292252,   8.99845123,   7.49668884],
              [  5.23528767,   9.80230141,   8.3005352 ],
              ...,
              [ -6.43249083,  -4.27642155,  -8.30053616],
              [ -5.19838905,  -3.47257614,  -7.49668884],
              [ 13.42047501,  -3.26066017,  -7.28477287]])
    >>> rtdata2.DataSet.GetClassName()
    'vtkUnstructuredGrid'

Gradient and algorithms that require access to a mesh work
whether that mesh is a uniform grid or a curvilinear grid or an
unstructured grid thanks to VTK’s data model. Take a look at
various functions in the algorithms module to see all the cool
things that can be accomplished using it. I will write future
blogs that demonstrate how specific problems can be solved
using these modules.

All of this work with composite datasets and in parallel using
MPI. I will cover some specific details about these in future
blogs.

Part 4
------

Welcome to another blog where we continue to discover VTK’s
numpy_interface module. If you are not familiar with this
module, I recommend checking out my previous blogs on it
([`1 <https://blog.kitware.com/improved-vtk-numpy-integration/>`__],
[`2 <https://blog.kitware.com/improved-vtk-numpy-integration-part-2/>`__],
[`3 <https://blog.kitware.com/improved-vtk-numpy-integration-part-3/>`__]).
In this blog, I will talk about how numpy_interface can be used
in a data parallel way. We will be using VTK’s MPI support for
this – through VTK’s own vtkMultiProcessController and mpi4py.
You may want to check `my last
blog <https://blog.kitware.com/mpi4py-and-vtk/>`__ on VTK’s
mpi4py integration for some details.

Let’s get started. First, if you want to run these examples
yourself, make sure that VTK is compiled with MPI support on by
setting VTK_Group_MPI to ON during the CMake configuration
step. The simplest way to run these examples is to use the
pvtkpython executable that gets compiled when Python and MPI
support are on. Pvtkpython can be run from the command line as
follows (check out your MPI documentation for details).

.. code:: python

    mpiexec -n 3 ./bin/pvtkpython

Now, let’s start with a simple example.

.. code:: python

    import vtk

    c = vtk.vtkMultiProcessController.GetGlobalController()

    rank = c.GetLocalProcessId()
    size = c.GetNumberOfProcesses()

    w = vtk.vtkRTAnalyticSource()
    w.UpdateInformation()
    w.SetUpdateExtent(rank, size, 0)
    w.Update()

    print w.GetOutput().GetPointData().GetScalars().GetRange()

When I run this example on my laptop, I get the following
output:

.. code:: python

    (37.35310363769531, 251.69105529785156)
    (49.75050354003906, 276.8288269042969)
    (37.35310363769531, 266.57025146484375)

Depending on a particular run and your OS, you may see
something similar or something a bit garbled. Since I didn’t
restrict the print call to a particular rank, all processes
will print out roughly at the same time and depending on the
timing and buffering, may end up mixing up with each others’
output. If you examine the output above, you will notice that
the overall range of the scalars is (37.35310363769531,
276.8288269042969), which we can confirm by running the example
serially.

Note that vtkRTAnalyticSource is a parallel-aware source and
produces partitioned data. The following lines are what tell
vtkRTAnalyticSource to produce its output in a distributed way

.. code:: python

    w = vtk.vtkRTAnalyticSource()

    # First we need to ask the source to produce
    # meta-data. Unless UpdateInformation() is
    # called first, SetUpdateExtent() below will
    # have no effect
    w.UpdateInformation()

    # Ask the source to produce "size" pieces and
    # select the piece of index "rank" for this process.
    w.SetUpdateExtent(rank, size, 0)

    # Cause execution. Note that the source has to
    # be updated directly for the above request to
    # work. Otherwise, downstream pipeline can overwrite
    # requested piece information.
    w.Update()

For more details, see `this
page <http://www.vtk.org/Wiki/VTK/Parallel_Pipeline>`__.

So how can we find out the global min and max of the scalars
(RTData) array? One way is to use mpi4py to perform a reduction
of local values of each process. Or we can use the
numpy_interface.algorithms module. Add the following code to
the end of our example.

.. code:: python

    from vtk.numpy_interface import dataset_adapter as dsa
    from vtk.numpy_interface import algorithms as algs

    w = dsa.WrapDataObject(w.GetOutput())
    rtdata = w.PointData['RTData']
    _min = algs.min(rtdata)
    _max = algs.max(rtdata)

    if rank == 0:
        print _min, _max

This should print the following.

.. code:: python

    37.3531036377 276.828826904

That simple! All algorithms in the numpy_interface.algorithms
module work properly in parallel. Note that min, max and any
other parallel algorithm will return the same value on all
ranks.

It is possible to force these algorithms to produce local
values by setting their controller argument as follows.

.. code:: python

    # vtkDummyController is a replacement for vtkMPIController
    # that works only locally to each rank.
    _min = algs.min(rtdata, controller = vtk.vtkDummyController())
    _max = algs.max(rtdata, controller = vtk.vtkDummyController())

    if rank == 0:
        print _min, _max

This will print the following.

.. code:: python

    37.3531036377 251.691055298

All algorithms in the numpy_interface.algorithms module were
designed to work in parallel. If you use numpy algorithms
directly, you will have to use mpi4py and do the proper
reduction.

One final thing. Numpy.dataset_adapter and numpy.algorithms
were designed to work properly even when an array does not
exist on one or more of the ranks. This occurs when a source
can produce a limited number of pieces (1 being the most common
case) and the size of the parallel job is larger. Let’s start
with an example:

.. code:: python

    import vtk

    c = vtk.vtkMultiProcessController.GetGlobalController()

    rank = c.GetLocalProcessId()
    size = c.GetNumberOfProcesses()

    c = vtk.vtkCubeSource()
    c.UpdateInformation()
    c.SetUpdateExtent(rank, size, 0)
    c.Update()

    from vtk.numpy_interface import dataset_adapter as dsa
    from vtk.numpy_interface import algorithms as algs

    c = dsa.WrapDataObject(c.GetOutput())
    normals = c.PointData['Normals']

    print normals

On my machine, this prints out the following.

.. code:: python

    <vtk.numpy_interface.dataset_adapter.VTKNoneArray object at 0x119176490>
    <vtk.numpy_interface.dataset_adapter.VTKNoneArray object at 0x11b128490>
    [[-1.  0.  0.]
    [-1.  0.  0.]
    [-1.  0.  0.]
    [-1.  0.  0.]
    ...
    [ 0.  0.  1.]
    [ 0.  0.  1.]]

Note how the normals is a VTKNoneArray on 2 of the ranks. This
is because vtkCubeSource is not parallel-aware and will produce
output only on the first rank. On all other ranks, it will
produce empty output. Consider the use case where we want to do
something like this with normals.

.. code:: python

    print algs.max(normals + 1)

One would expect that this would throw an exception on ranks
where the normals array does not exist. In fact, the first
implementation of the numpy_interface.dataset_adapter returned
a None object and threw an exception in such cases as expected.
However, this design had a significant flaw. Because of the
exception, ranks that did not have the array could not
participate in the calculation of global values, which are
calculated by performing MPI_Allreduce. This function will hang
unless all ranks participate in the reduction. We addressed
this flaw by developing the VTKNoneArray class. This class
supports all operators that regular arrays support and always
returns VTKNoneArray. Furthermore, parallel algorithms function
properly when asked to work on a VTKNoneArray.

We have covered a lot ground so far. In the next blog, which
will be the last one in this series, I will talk about
composite datasets and composite arrays.

Part 5
------

Welcome to my last blog in the series where we to discover
VTK’s numpy_interface module. If you are not familiar with this
module, I recommend checking out my previous blogs on it
([`1 <https://blog.kitware.com/improved-vtk-numpy-integration/>`__],
[`2 <https://blog.kitware.com/improved-vtk-numpy-integration-part-2/>`__],
[`3 <https://blog.kitware.com/improved-vtk-numpy-integration-part-3/>`__]).
In this blog, I will talk about how one can work with composite
datasets and arrays using this module.

Let’s start with defining what a composite dataset is. From a
class point of view, it is vtkCompositeDataSet or any of its
subclasses. From a functionality point of view, it is a way of
collecting together a set of vtkDataObjects (usually
vtkDataSets). The most generic example is vtkMultiBlockDataSet
which allows the creation of an arbitrary tree of
vtkDataObjects. Another example is vtkOverlappingAMR which
represent a Berger-Colella style AMR meshes. Here is how we can
create a multi-block dataset.

.. code:: python

    >>> import vtk
    >>> s = vtk.vtkSphereSource()
    >>> s.Update()
    >>> c = vtk.vtkConeSource()
    >>> c.Update()
    >>> mb = vtk.vtkMultiBlockDataSet()
    >>> mb.SetBlock(0, s.GetOutput())
    >>> mb.SetBlock(1, c.GetOutput())

Many of VTK’s algorithms work with composite datasets without
any change. For example:

.. code:: python

    >>> e = vtk.vtkElevationFilter()
    >>> e.SetInputData(mb)
    >>> e.Update()
    >>> mbe = e.GetOutputDataObject(0)
    >>> print mbe.GetClassName()

This will output ‘vtkMultiBlockDataSet’. Note that I used
GetOutputDataObject() rather than GetOutput(). GetOutput() is
simply a GetOutputDataObject() wrapped with a SafeDownCast() to
the expected output type of the algorithm – which in this case
is a vtkDataSet. So GetOutput() will return 0 even when
GetOutputDataObject() returns an actual composite dataset.

Now that we have a composite dataset with a scalar, we can use
numpy_interface.

.. code:: python

    >>> from vtk.numpy_interface import dataset_adapter as dsa
    >>> mbw = dsa.WrapDataObject(mbe)
    >>> mbw.PointData.keys()
    ['Normals', 'Elevation']
    >>> elev = mbw.PointData['Elevation']
    >>> elev
    <vtk.numpy_interface.dataset_adapter.VTKCompositeDataArray at 0x1189ee410>

Note that the array type is different than we have seen
previously (VTKArray). However, it still works the same way.

.. code:: python

    >>> from vtk.numpy_interface import algorithms as algs
    >>> algs.max(elev)
    0.5
    >>> algs.max(elev + 1)
    1.5

You can individually access the arrays of each block as
follows.

.. code:: python

    >>> elev.Arrays[0]
    VTKArray([ 0.5       ,  0.        ,  0.45048442,  0.3117449 ,  0.11126047,
               0.        ,  0.        ,  0.        ,  0.45048442,  0.3117449 ,
               0.11126047,  0.        ,  0.        ,  0.        ,  0.45048442,
               0.3117449 ,  0.11126047,  0.        ,  0.        ,  0.        ,
               0.45048442,  0.3117449 ,  0.11126047,  0.        ,  0.        ,
               0.        ,  0.45048442,  0.3117449 ,  0.11126047,  0.        ,
               0.        ,  0.        ,  0.45048442,  0.3117449 ,  0.11126047,
               0.        ,  0.        ,  0.        ,  0.45048442,  0.3117449 ,
               0.11126047,  0.        ,  0.        ,  0.        ,  0.45048442,
               0.3117449 ,  0.11126047,  0.        ,  0.        ,  0.        ], dtype=float32)

Note that indexing is slightly different.

.. code:: python

    >>> print elev[0:3]
    [VTKArray([ 0.5,  0.,  0.45048442], dtype=float32),
     VTKArray([ 0.,  0.,  0.43301269], dtype=float32)]

The return value is a composite array consisting of 2
VTKArrays. The [] operator simply returned the first 4 values
of each array. In general, all indexing operations apply to
each VTKArray in the composite array collection. Similarly for
algorithms such as where.

.. code:: python

    >>> print algs.where(elev < 0.5)
    [(array([ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17,
             18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
             35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]),),
             (array([0, 1, 2, 3, 4, 5, 6]),)]

Now, let’s look at the other array called Normals.

.. code:: python

    >>> normals = mbw.PointData['Normals']
    >>> normals.Arrays[0]
    VTKArray([[  0.00000000e+00,   0.00000000e+00,   1.00000000e+00],
              [  0.00000000e+00,   0.00000000e+00,  -1.00000000e+00],
              [  4.33883727e-01,   0.00000000e+00,   9.00968850e-01],
              [  7.81831503e-01,   0.00000000e+00,   6.23489797e-01],
              [  9.74927902e-01,   0.00000000e+00,   2.22520933e-01],
              ...
              [  6.89378142e-01,  -6.89378142e-01,   2.22520933e-01],
              [  6.89378142e-01,  -6.89378142e-01,  -2.22520933e-01],
              [  5.52838326e-01,  -5.52838326e-01,  -6.23489797e-01],
              [  3.06802124e-01,  -3.06802124e-01,  -9.00968850e-01]], dtype=float32)
    >>> normals.Arrays[1]
    <vtk.numpy_interface.dataset_adapter.VTKNoneArray at 0x1189e7790>

Notice how the second arrays is a VTKNoneArray. This is because
vtkConeSource does not produce normals. Where an array does not
exist, we use a VTKNoneArray as placeholder. This allows us to
maintain a one-to-one mapping between datasets of a composite
dataset and the arrays in the VTKCompositeDataArray. It also
allows us to keep algorithms working in parallel without a lot
of specialized code (see my `previous
blog <https://blog.kitware.com/improved-vtk-numpy-integration-part-4/>`__).

Where many of the algorithms apply independently to each array
in a collection, some algorithms are global. For example, min
and max as we demonstrated above. It is sometimes useful to get
per block answers. For this, you can use \_per_block
algorithms.

.. code:: python

    >>> print algs.max_per_block(elev)
    [VTKArray(0.5, dtype=float32), VTKArray(0.4330126941204071, dtype=float32)]

These work very nicely together with other operations. For
example, here is how we can normalize the elevation values in
each block.

.. code:: python

    >>> _min = algs.min_per_block(elev)
    >>> _max = algs.max_per_block(elev)
    >>> _norm = (elev - _min) / (_max - _min)
    >>> print algs.min(_norm)
    0.0
    >>> print algs.max(_norm)
    1.0

Once you grasp these features, you should be able to use
composite array very similarly to single arrays as described in
previous blogs.

A final note on composite datasets. The composite data wrapper
provided by numpy_interface.dataset_adapter offers a few
convenience functions to traverse composite datasets. Here is a
simple example:

.. code:: python

    >>> for ds in mbw:
    >>>    print type(ds)
    <class 'vtk.numpy_interface.dataset_adapter.PolyData'>
    <class 'vtk.numpy_interface.dataset_adapter.PolyData'>

This wraps up the blog series on numpy_interface. I hope to
follow these up with some concrete examples of the module in
action and some other useful information on using VTK from
Python. Until then, cheers.
