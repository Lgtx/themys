Documentation: `doc fr <../../fr/procedures/TimeManagement.html>`__



Time list management
====================

Description
-----------

Themys/ParaView allows to open several simulation times.

By default, when a new database is opened, it contributes to the list of
times offered in ParaView.

When a time is selected, this has the effect of positioning all the
bases at the value closest to this chosen time, value from below.

The **Time Inspector** available in the views lets you choose the list of
times for a database, all or part of the databases.

The choice of the list of times then also impacts the animation view.

There are filters that allow you to build new lists of times from others
such as :

# Extract Time Steps,

# MergeTime (CEA development).
