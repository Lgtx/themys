Documentation: `doc fr <../../fr/procedures/ExportProfile.html>`__



Export a profile as a CSV file
==============================

Description
-----------

Generating a data profile in Themys is useful but one may want to export
it and visualize it outside of Themys. It is possible using the CSV
format.

Procedure using the GUI
-----------------------

-  Load your data into Themys and display it
-  Create the profile using the ``Plot Over Line`` filter.

   -  To setup the position of your profile, edit the parameters in the
      ``Properties`` section.
   -  A more thorough explanation on how to create profile can be found
      on `this page <PlotOverLineCustom.md>`__

-  Apply. On first apply Themys will automatically create a chart view
   to visualize the resulting profile. You can change the parameters of
   the filter until you are happy with the result of your profile.
-  Make sure the advanced mode of Themys is toggled on.
-  Once satisfied with your profile, you can now export it. There are 2
   methods for that : using ``File - Save Data`` or using the
   spreadsheet view.

Using ‘File - Save Data’
~~~~~~~~~~~~~~~~~~~~~~~~

-  With the ``Plot Over Line`` filter selected, go to
   ``File - Save Data``.
-  On the first dialog Themys will ask you a location where to save the
   file and the type of the file you want to write. Choose
   ``Comma or  Tab Delimited Files`` and click ``Ok``.
-  A second dialog will open where you can set some parameters :

   -  ``Choose array to Write`` : allows you to chose which data to
      export into your CSV profile. Note that ``arc_length`` should
      always be exported as it refers to the length along your profile
      and is used as the X axis.
   -  ``Precision`` : number of digits for floating point numbers.
   -  ``Field Association`` : should always be ‘Point Data’.

-  Note that all 3D point coordinates of the profile will always be
   exported using this method.

Using the spreadsheet view
~~~~~~~~~~~~~~~~~~~~~~~~~~

-  In Themys open a new visualisation tab (or layout) by clicking the
   ``+`` tab next to your current layout.
-  Choose ‘Spreadsheet view’.
-  On top of the spreadsheet view make sure that ``Showing`` has your
   Plot Over Line filter selected. You now should be able to see your
   profile in the spreadsheet view.
-  Choose which array to display and export using the
   ``Toggle column visibility`` button on top of the spreadsheet view.
-  Next to the ``Toggle column visibility`` button, click on the
   ``Export Spreadsheet`` button.
-  On the first dialog Themys will ask you a location where to save the
   file.
-  A second dialog will open where you can set some parameters :

   -  ``Real Number Notation`` : how to write numbers. Three modes are
      available :

      -  ``Mixed`` : will automatically choose the shortest display
      -  ``Fixed`` : use fixed number of digits for floating point
         numbers.
      -  ``Scientific`` : use scientific notation

   -  ``Real Number Precision`` : number of digits for floating point
      numbers.

|image0|

Procedure using Python scripting
--------------------------------

.. code:: py

   from paraview.simple import *

   # Create your data. This can ever be by reading a file or creating a source.
   # Here for this example we create a Wavelet source
   data = Wavelet()

   # create a new 'Plot Over Line' filter that takes your data as input.
   profileFilter = PlotOverLine(Input=data)
   # you can customize the resolution of your profile, as well as its starting and ending point for example.
   profileFilter.Resolution = 100
   profileFilter.Point1 = [-5.0, -5.0, -10.0]
   profileFilter.Point2 = [5.0, 10.0, 10.0]

   # create a new 'SpreadSheet View' so we can export our data from there
   spreadSheet = CreateView('SpreadSheetView')

   # show the profile in the spreadsheet view
   Show(profileFilter, spreadSheet, 'SpreadSheetRepresentation')

   # hide unwanted data.
   # Below is an example of data we may not want to export when doing the profile of a wavelet
   spreadSheet.HiddenColumnLabels = ['Block Number', 'Point ID', 'Points', 'Points_Magnitude', 'vtkValidPointMask']

   # export the profile.
   # Note the parameters that allows us to custom floating point numbers writing.
   ExportView('/home/user/profile.csv', view=spreadSheet, RealNumberNotation='Fixed', RealNumberPrecision=7)

.. |image0| image:: ../../img/procedures/SpreadsheetViewButtons.png
