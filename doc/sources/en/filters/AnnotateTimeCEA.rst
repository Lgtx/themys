Documentation: `doc fr <../../fr/filters/AnnotateTimeCEA.html>`__

Annotate Time CEA filter
========================

This filter displays, in the render view, the value of the currently loaded timestep and its index value.

Only the **Expression** property is available for this filter. Its default value is:

``'Temps %f $\mu$s (#%d)' % (vtkFixedTimeValue*1e6, vtkFixedTimeStep)``

Here the time is considered to expressed in micro-seconds (1.e-6 second).

.. Comment for devs:
.. If the unit greek letter is not correctly displayed, there may be a problem in access or install
.. of the matplotlib module used by **Themys**
