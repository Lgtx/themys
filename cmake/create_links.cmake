# This function create links from the origin doc files written in origin lang to
# doc files written in destination lang. It does so only if the link is not
# already present. Both doc files should be located in a directory named with
# the value of origin_lang (fr for example) and dest_lang (en for example)
# sources |- fr |- - file_a.rst |- - file_b.rst |- en |- - file_a.rst |- -
# file_b.rst
#
# Doc files should have '.rst' extensions.
#
# Parameters: - origin_lang: origin language ("fr" or "en") - dest_lang:
# destination language ("fr" or "en") - origin_doc_files: list of origin
# documentation files
function(create_links origin_lang dest_lang origin_doc_files)
  message(
    STATUS
      "Creating links from ${origin_lang} language to ${dest_lang} language")

  foreach(doc_file ${origin_doc_files})
    get_filename_component(doc_file_ext ${doc_file} LAST_EXT)

    if(NOT ${doc_file_ext} STREQUAL ".rst")
      # Do not handle other files than .rst ones
      continue()
    endif()

    get_filename_component(doc_file_dir ${doc_file} DIRECTORY)
    get_filename_component(doc_file_name ${doc_file} NAME)

    # Replace fr by en (or en by fr) in the path file
    string(REGEX REPLACE "(.*/)${origin_lang}(/.*)" "\\1${dest_lang}\\2"
                         link_tgt ${doc_file})

    if(NOT EXISTS ${link_tgt})
      message(WARNING "The file ${link_tgt} is missing. "
                      "No links toward this missing file will be created!")
      continue()
    endif()

    # Get the relative path from fr file to en one (or inversely)
    file(RELATIVE_PATH rel_tgt ${doc_file_dir} ${link_tgt})
    # The link will be against the produced html files because both rst files
    # (en and fr) do not belong to the same sphinx project
    string(REPLACE ".rst" ".html" html_tgt ${rel_tgt})

    set(link_string "Documentation: `doc ${dest_lang} <${html_tgt}>`__")

    # Retrive the first line of the doc file to see if the link is not already
    # present
    file(STRINGS ${doc_file} doc_lines)
    list(GET doc_lines 0 first_line)

    # Do not modify current file if the link is already here
    if(DEFINED first_line)
      if("${first_line}" STREQUAL ${link_string})
        continue()
      endif()
    endif()

    # Write the modified file into a temporary one and replace origin
    file(READ ${doc_file} doc_content)
    file(WRITE "/tmp/${doc_file_name}" "${link_string}\n\n")
    file(APPEND "/tmp/${doc_file_name}" "${doc_content}")
    file(COPY_FILE /tmp/${doc_file_name} ${doc_file})
  endforeach()
endfunction()
