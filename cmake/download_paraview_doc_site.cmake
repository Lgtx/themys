# -------------------------------------------------------------------------------
# This cmake module will download the paraview's documentation website into the
# DOCUMENTATION_BUILD_DIR directory
# -------------------------------------------------------------------------------
if(NOT DEFINED DOCUMENTATION_BUILD_DIR)
  message(FATAL_ERROR "DOCUMENTATION_BUILD_DIR variable is undefined")
endif()

message(STATUS "Testing internet access...")
# Taken from
# https://stackoverflow.com/questions/62214621/how-to-check-for-internet-connection-with-cmake-automatically-prevent-fails-if
execute_process(
  COMMAND ping www.google.com -c 2
  ERROR_QUIET
  RESULT_VARIABLE PING_OUTPUT)

if(NOT PING_OUTPUT EQUAL 0)
  set(FULLY_DISCONNECTED ON)
  message(WARNING "Offline mode! ParaView's documentation won't be fetched!")
else()
  message(STATUS "Online mode! ParaView's documentation will be made available")
  set(FULLY_DISCONNECTED OFF)
endif()

if(NOT FULLY_DISCONNECTED)
  set(PARAVIEW_DOWNLOAD_SCRIPT download_pvdoc.cmake)
  set(PARAVIEW_DOC_ARCHIVE_NAME paraviewdoc.zip)

  # Write a script that will download the archive
  file(
    GENERATE
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${PARAVIEW_DOWNLOAD_SCRIPT}
    CONTENT
      "file(DOWNLOAD https://docs.paraview.org/_/downloads/en/latest/htmlzip/ ${PARAVIEW_DOC_ARCHIVE_NAME})"
  )

  # Adds a command that will execute the downloading script
  add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${PARAVIEW_DOC_ARCHIVE_NAME}
    COMMENT "Downloading ParaView's documentation site"
    COMMAND ${CMAKE_COMMAND} -P
            ${CMAKE_CURRENT_BINARY_DIR}/${PARAVIEW_DOWNLOAD_SCRIPT})

  # Adds a command that will extract the archive
  add_custom_command(
    OUTPUT ${DOCUMENTATION_BUILD_DIR}/paraview-latest
    COMMENT "Extracting ParaView's documentation site"
    COMMAND ${CMAKE_COMMAND} -E tar -xf
            ${CMAKE_CURRENT_BINARY_DIR}/${PARAVIEW_DOC_ARCHIVE_NAME}
    WORKING_DIRECTORY ${DOCUMENTATION_BUILD_DIR}
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PARAVIEW_DOC_ARCHIVE_NAME})

  # Adds a target that will trigger both of the above commands
  add_custom_target(
    GetParaViewDocSite
    COMMENT "Retrieving ParaView's documentation site"
    DEPENDS ${DOCUMENTATION_BUILD_DIR}/paraview-latest)
else()
  # Adds a target that does nothing. It avoids to add a test when adding this
  # target as a dependency of themys target
  add_custom_target(GetParaViewDocSite
                    COMMENT "Not retrieving ParaView's documentation site")
endif()

# No need to install rule because the website is extracted in the
# DOCUMENTATION_BUILD_DIR directory which has its own install rule
